# Swissfaustball Schiko Bundle

## About
Mit diesem Bundle kann die Schiedsrichterkommission von Swiss Faustball (SCHIKO) sämtliche Schiedsrichter, Einsätze und Abrechnungen
koordinieren. 

### Features
- [x] Verwaltung on nationalen und internationalen Schiedsrichter der Schweiz
- [x] Ausschreibungen von Einsätzen für die nationalen Meisterschaften und die internationalen Anlässe in der Schweiz
- [x] Einteilung der Schiedsrichter für die nationalen Meisterschaften und die internationalen Anlässe in der Schweiz
- [x] Abrechnung der Schiedsrichtereinsätze mit den Schiedsrichtern, den Organisatoren von Spiel-tagen und den betroffenen Kommissionen (M-KO, F-KO, LIKO, JUKO)
- [x] Automatisches Geocoding für Adressen und Distanzen über geocoding service

## Installation
Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo (not on packagist.org) to your composer.json:
```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/swissfaustball-schiko-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/swissfaustball-schiko-bundle": "dev-master",
```

## Usage (German)
ACHTUNG: Bitte Google Maps API Key in den Contao Settings hinterlegen.

### Folgende Inserttags stehen zur Verfügung


## Contribution
Bug reports and pull requests are welcome

