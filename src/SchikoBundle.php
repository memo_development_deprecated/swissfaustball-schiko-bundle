<?php

/**
 * @package   SCHIKO Bundle
 * @author    Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\SchikoBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SchikoBundle extends Bundle {

}
