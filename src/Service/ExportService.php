<?php

namespace Memo\SchikoBundle\Service;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\File;
use Contao\FilesModel;
use Doctrine\DBAL\Connection;
use Memo\ModClubBundle\Model\ClubModel;
use NotificationCenter\Util\StringUtil;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Contao\FrontendUser;
use Contao\MemberModel;
use Contao\Request;
use Contao\System;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Memo\SchikoBundle\Model\SchikoMemberModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooterDrawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Worksheet\HeaderFooter;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExportService
{

    private $framework;
    private $request;
    private $db;
    private $fontSize       = 11;
    private $fillColor      = '230, 230, 230';
    private $tmpSavePath    = '/files/export/';
    private $aColumnHeaders = [
        'lastname' => 'Name',
        'firstname' => 'Vorname',
        'dateOfBirth' => 'Geb.',
        'street' => 'Strasse',
        'postal' => 'PLZ',
        'city' => 'Ort',
        'phone' => 'Tel. P',
        'mobile' => 'Tel. Mobil',
        'phone_business' => 'Tel. G',
        'brevet_year' => 'BJ',
        'entry_field' => 'EF',
        'entry_hall' => 'EH',
        'entry_total' => 'Tot',
        'club' => 'Verein',
        'count_for_club' => 'Einsätze zählen für Verein',
        'email' => 'E-Mail'
    ];


    /**
     * @param ContaoFramework $framework
     * @param RequestStack $requestStack
     */
    public function __construct(ContaoFramework $framework,RequestStack $requestStack, Connection $db)
    {
        $this->framework = $framework;
        $this->request  = $requestStack;
        $this->db  = $db;
        $this->framework->initialize();
    }

    /**
     * @param int $season_id
     * @return void
     */
    public function exportSeasonBillingXLS(int $season_id)
    {
        if(empty($season_id))
        {
            \Contao\Controller::redirect('/contao?do=tl_sf_season');
        }

        //Get Data
        $oSeason = SfSeasonModel::findByPk($season_id);
        $spreadsheet = new Spreadsheet();

        //Add Logo
        $drawing = new HeaderFooterDrawing();
        $drawing->setName('Swissfaustball Logo');
        $drawing->setPath(dirname(dirname(__FILE__)).'/Resources/public/vorlagen/logo.jpg');

        $iLineNo = 1;
        $sType = !empty($oSeason->type) && $oSeason->type == 1? 'Feld' : 'Halle';
        $spreadsheet->getActiveSheet()->setTitle('Vereine');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->addImage($drawing, HeaderFooter::IMAGE_HEADER_LEFT);
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&G');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&Abrechnung-Saison: '.$oSeason->season.' '.$sType.'&Cwww.swissfaustball.ch&R&D');

        //Settings
        $sheet       = $spreadsheet->getActiveSheet();
        $sheet->getPageSetup()->setFitToPage(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);
        $tblStyleGutter = ['allBorders' => ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN, 'color' => array('argb' => 'FF000000')]];
        $styleCellArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ]
            ]
        ];

        //Create Totals Sheet
        $spreadsheet->createSheet();
        $cSheet       = $spreadsheet->getSheet(1);
        $spreadsheet->createSheet();
        $tSheet       = $spreadsheet->getSheet(2);

        //Total Page Setup
        $iTotalLineNo = 1;
        $tSheet->setTitle('Totals');
        $tSheet->mergeCells("A".$iTotalLineNo.":I".$iTotalLineNo);

        $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue('Total Saison: '.$oSeason->season.' '.$sType); //Titel
        $tSheet->getStyle('A'.$iTotalLineNo.':I'.$iTotalLineNo)->getFont()->setSize(18);
        $tSheet->getStyle('A'.$iTotalLineNo.':I'.$iTotalLineNo)->getFont()->setBold(true);
        $iTotalLineNo++;
        $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue('Vereine'); //Titel
        $sheet->getCell(Coordinate::stringFromColumnIndex(4). $iLineNo)->setValue('Kommissionen'); //Titel
        $tSheet->getStyle('A'.$iTotalLineNo.':I'.$iTotalLineNo)->getFont()->setSize(18);
        $tSheet->getStyle('A'.$iTotalLineNo.':I'.$iTotalLineNo)->getFont()->setBold(true);
        $sheet->getStyle('A' . $iTotalLineNo . ':I' . $iTotalLineNo)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('A' . $iTotalLineNo . ':I' . $iTotalLineNo)->getFill()->getStartColor()->setRGB('efefef');

        $iTotalLineNo++;
        //-----------------------------------

        $sheet->mergeCells("A".$iLineNo.":I".$iLineNo);
        $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue('Abrechnung Vereine Saison: '.$oSeason->season.' '.$sType); //Titel
        $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setSize(20);
        $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setBold(true);
        $iLineNo++;

        //Get Verein Data
        $arrClubData = $this->_getClubsBySeason($oSeason->id);

        $iClubLineNo = null;
        foreach($arrClubData as $club_id => $arrGames) {

            //Add Verein Headline
            $iLineNo++;
            $iClubLineNo = $iLineNo;
            $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue($arrGames[0]['club_name']);
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setSize(16);
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setBold(true);
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFill()->getStartColor()->setRGB('efefef');
            foreach ($sheet->getColumnIterator() as $column) {
                $sheet->getStyle($column->getColumnIndex().$iLineNo)->applyFromArray($styleCellArray);
                //Break if last Column enter
                if($column->getColumnIndex() == "I")
                {
                    break;
                }
            }
            $iLineNo++;
            //add Verein to toals page
            $tSheet->getCellByColumnAndRow(1, $iTotalLineNo)->setValue($arrGames[0]['club_name']);

            $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue('Datum');
            $sheet->getCell(Coordinate::stringFromColumnIndex(2). $iLineNo)->setValue('Kategorie');
            $sheet->getCell(Coordinate::stringFromColumnIndex(3). $iLineNo)->setValue('Ort');
            $sheet->getCell(Coordinate::stringFromColumnIndex(4). $iLineNo)->setValue('#Schiedsrichter');
            $sheet->getCell(Coordinate::stringFromColumnIndex(5). $iLineNo)->setValue('Taggeld');
            $sheet->getCell(Coordinate::stringFromColumnIndex(6). $iLineNo)->setValue('Reisespesen');
            $sheet->getCell(Coordinate::stringFromColumnIndex(7). $iLineNo)->setValue('Porto');
            $sheet->getCell(Coordinate::stringFromColumnIndex(8). $iLineNo)->setValue('Bonus');
            $sheet->getCell(Coordinate::stringFromColumnIndex(9). $iLineNo)->setValue('Total');
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setSize(14);
            $sheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setBold(true);

            $intSummClub = 0;
            $SchikoSurveyModel = $this->framework->createInstance('Memo\SchikoBundle\Model\SchikoSurveyModel');
            foreach($arrGames as $gKey => $game) {
                $oData = $SchikoSurveyModel->findByRound($game['round_id']);
                if(empty($oData->referee))
                {
                    continue;
                }
                $iReferee = 0;
                $iFee = 0;
                $iTravel = 0;
                $iBonus = 0;
                $iPorto = 0;
                $iTotal = 0;
                foreach ($oData->referee as $key => $oReferee) {
                    $iFee += $oReferee->fee;
                    $iTravel += $oReferee->travel_cost;
                    $iBonus += empty($oReferee->bonus)? 0 : $oReferee->bonus;
                    $iPorto += empty($oReferee->porto)? 0 : $oReferee->porto;
                    $iReferee++;
                }
                $iTotal += empty($oReferee->total)? 0 : $iFee+$iTravel+$iBonus+$iPorto;
                $intSummClub += $iTotal;
                $iLineNo++;
                $sheet->getCell(Coordinate::stringFromColumnIndex(1). $iLineNo)->setValue(date("d.m.Y", $oData->date));
                $sheet->getCell(Coordinate::stringFromColumnIndex(2). $iLineNo)->setValue($game['category']);
                $sheet->getCell(Coordinate::stringFromColumnIndex(3). $iLineNo)->setValue($oData->location);
                $sheet->getCell(Coordinate::stringFromColumnIndex(4). $iLineNo)->setValue($iReferee);
                $sheet->getCell(Coordinate::stringFromColumnIndex(5). $iLineNo)->setValue(number_format($iFee));
                $sheet->getCell(Coordinate::stringFromColumnIndex(6). $iLineNo)->setValue(number_format($iTravel));
                $sheet->getCell(Coordinate::stringFromColumnIndex(7). $iLineNo)->setValue(number_format($iPorto));
                $sheet->getCell(Coordinate::stringFromColumnIndex(8). $iLineNo)->setValue(number_format($iBonus));
                $sheet->getCell(Coordinate::stringFromColumnIndex(9). $iLineNo)->setValue(number_format($iTotal));
            }
            //Add Club Total Value
            $sheet->mergeCells("H".$iClubLineNo.":I".$iClubLineNo);
            $sheet->mergeCells("A".$iClubLineNo.":G".$iClubLineNo);
            $sheet->getCell(Coordinate::stringFromColumnIndex(8). $iClubLineNo)->setValue('CHF '.number_format($intSummClub,2,".","'"));
            $sheet->getStyle('H' . $iClubLineNo . ':I' . $iClubLineNo)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheet->getStyle('A'.$iClubLineNo.':I'.$iLineNo)->getBorders()->applyFromArray($tblStyleGutter);
            $iLineNo++;

            //Add Club to Totals Page
            $tSheet->getCell(Coordinate::stringFromColumnIndex(2) . $iTotalLineNo)->setValue(intval($intSummClub));
            $tSheet->getStyle('H' . $iTotalLineNo . ':I' . $iTotalLineNo)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $iTotalLineNo++;
        }


        //New Kommission Sheet
        $iLineNo = 1;
        $cSheet->setTitle('Kommission');
        $cSheet->mergeCells("A".$iLineNo.":I".$iLineNo);

        $cSheet->getCell(Coordinate::stringFromColumnIndex(1).$iLineNo)->setValue('Abrechnung Kommissionen Saison: '.$oSeason->season.' '.$sType); //Titel
        $cSheet->getStyle('A'.$iLineNo.':I'.$iLineNo)->getFont()->setSize(18);
        $cSheet->getStyle('A'.$iLineNo.':I'.$iLineNo)->getFont()->setBold(true);

        $cSheet->getColumnDimension('A')->setAutoSize(true);
        $cSheet->getColumnDimension('B')->setAutoSize(true);
        $cSheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);



        //Get Kommission Data
        $arrCommissions = $this->_getCommissionBillingData($season_id);

        $iCommissionLineNo = null;
        $iTotalCommissionLineNo = 3;
        foreach($arrCommissions as $comId => $arrGames) {

            //Add Verein Headline
            $iLineNo++;
            $iCommissionLineNo = $iLineNo;
            $cSheet->getCell(Coordinate::stringFromColumnIndex(1) . $iLineNo)->setValue($arrGames[0]['schiko_name']);
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setSize(16);
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setBold(true);
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFill()->getStartColor()->setRGB('efefef');
            foreach ($cSheet->getColumnIterator() as $column) {
                $cSheet->getStyle($column->getColumnIndex().$iLineNo)->applyFromArray($styleCellArray);
                //Break if last Column enter
                if($column->getColumnIndex() == "I")
                {
                    break;
                }
            }
            $iLineNo++;

            $cSheet->getCell(Coordinate::stringFromColumnIndex(1) . $iLineNo)->setValue('Datum');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(2) . $iLineNo)->setValue('Kategorie');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(3) . $iLineNo)->setValue('Ort');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(4) . $iLineNo)->setValue('#Schiedsrichter');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(5) . $iLineNo)->setValue('Taggeld');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(6) . $iLineNo)->setValue('Reisespesen');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(7) . $iLineNo)->setValue('Porto');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(8) . $iLineNo)->setValue('Bonus');
            $cSheet->getCell(Coordinate::stringFromColumnIndex(9) . $iLineNo)->setValue('Total');
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setSize(14);
            $cSheet->getStyle('A' . $iLineNo . ':I' . $iLineNo)->getFont()->setBold(true);

            $intSummCommission = 0;
            foreach($arrGames as $gKey => $game) {

                $oData = $SchikoSurveyModel->findByRound($game['round_id']);
                if(empty($oData->referee))
                {
                    continue;
                }
                $iReferee = 0;
                $iFee = 0;
                $iTravel = 0;
                $iBonus = 0;
                $iPorto = 0;
                $iTotal = 0;
                foreach ($oData->referee as $key => $oReferee) {
                    $iFee += $oReferee->fee;
                    $iTravel += $oReferee->travel_cost;
                    $iBonus += empty($oReferee->bonus)? 0 : $oReferee->bonus;
                    $iPorto += empty($oReferee->porto)? 0 : $oReferee->porto;
                    $iReferee++;
                }
                $iTotal += empty($oReferee->total)? 0 : $iFee+$iTravel+$iBonus+$iPorto;

                $intSummCommission += $iTotal;
                $iLineNo++;

                $cSheet->getCell(Coordinate::stringFromColumnIndex(1) . $iLineNo)->setValue(date("d.m.Y", $oData->date));
                $cSheet->getCell(Coordinate::stringFromColumnIndex(2) . $iLineNo)->setValue($game['category']);
                $cSheet->getCell(Coordinate::stringFromColumnIndex(3) . $iLineNo)->setValue($oData->location);
                $cSheet->getCell(Coordinate::stringFromColumnIndex(4) . $iLineNo)->setValue($iReferee);
                $cSheet->getCell(Coordinate::stringFromColumnIndex(5) . $iLineNo)->setValue(number_format($iFee));
                $cSheet->getCell(Coordinate::stringFromColumnIndex(6) . $iLineNo)->setValue(number_format($iTravel));
                $cSheet->getCell(Coordinate::stringFromColumnIndex(7) . $iLineNo)->setValue(number_format($iPorto));
                $cSheet->getCell(Coordinate::stringFromColumnIndex(8) . $iLineNo)->setValue(number_format($iBonus));
                $cSheet->getCell(Coordinate::stringFromColumnIndex(9) . $iLineNo)->setValue(number_format($iTotal));
            }

            //Add Club Total Value
            $cSheet->mergeCells("A".$iCommissionLineNo.":G".$iCommissionLineNo);
            $cSheet->mergeCells("H".$iCommissionLineNo.":I".$iCommissionLineNo);
            $cSheet->getCell(Coordinate::stringFromColumnIndex(8). $iCommissionLineNo)->setValue('CHF '.number_format($intSummCommission,2,".","'"));
            $cSheet->getStyle('H' . $iCommissionLineNo . ':I' . $iCommissionLineNo)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $cSheet->getStyle('A'.$iCommissionLineNo.':I'.$iLineNo)->getBorders()->applyFromArray($tblStyleGutter);
            $iLineNo++;

            //Add Commissions to Totals Page
            $tSheet->getCellByColumnAndRow(4, $iTotalCommissionLineNo)->setValue($arrGames[0]['schiko_name']);
            $tSheet->getCellByColumnAndRow(5, $iTotalCommissionLineNo)->setValue(intval($intSummCommission));
            $tSheet->getStyle('H' . $iTotalCommissionLineNo . ':I' . $iTotalCommissionLineNo)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $iTotalCommissionLineNo++;
        }


        //Finalize TotalPage
        define('CURRENCY_FORMAT','##0.00;##0.00;');
        $sumClub = 'B3:B'.$iTotalLineNo;
        $sumCom = 'E3:E'.$iTotalCommissionLineNo;
        $iTotalCommissionLineNo++;
        $iTotalLineNo++;
        $tSheet->setCellValue('A'.$iTotalLineNo,"Total");
        $tSheet->setCellValue('B'.$iTotalLineNo , "=SUM($sumClub)");
        $tSheet->getStyle('A' . $iTotalLineNo . ':B' . $iTotalLineNo)->getFont()->setSize(16);
        $tSheet->getStyle('A' . $iTotalLineNo . ':B' . $iTotalLineNo)->getFont()->setBold(true);

        $tSheet->setCellValue('D'.$iTotalCommissionLineNo,"Total");
        $tSheet->setCellValue('E'.$iTotalCommissionLineNo , "=SUM($sumCom)");
        $tSheet->getStyle('D' . $iTotalCommissionLineNo . ':E' . $iTotalCommissionLineNo)->getFont()->setSize(16);
        $tSheet->getStyle('D' . $iTotalCommissionLineNo . ':E' . $iTotalCommissionLineNo)->getFont()->setBold(true);
        $tSheet->getStyle('E3:E'.$iTotalCommissionLineNo)->getNumberFormat()->setFormatCode(CURRENCY_FORMAT);
        $tSheet->getStyle('B3:B'.$iTotalLineNo)->getNumberFormat()->setFormatCode(CURRENCY_FORMAT);
        $tSheet->getColumnDimension('A')->setAutoSize(true);
        $tSheet->getColumnDimension('B')->setAutoSize(true);
        $tSheet->getColumnDimension('D')->setAutoSize(true);
        $tSheet->getColumnDimension('E')->setAutoSize(true);

        $tSheet->getStyle('A3:B'.$iTotalLineNo)->getBorders()->applyFromArray($tblStyleGutter);
        $tSheet->getStyle('D3:E'.$iTotalCommissionLineNo)->getBorders()->applyFromArray($tblStyleGutter);

        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
        $sheet->getPageSetup()->setFitToWidth(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        $cSheet->getPageSetup()->setFitToWidth(1);
        $cSheet->getPageSetup()->setFitToHeight(0);


        //Get Filename and output to browser
        $fileName = date("Y-m-d_")."Abrechnung-Saison".$oSeason->season.".xls";
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');




        \Contao\Controller::redirect('/contao?do=tl_sf_season');
    }

    /**
     * @param bool $blnSavePdf
     * @return Response
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportDirectoryXLS(bool $blnSavePdf = false): Response {

        //Get Data
        $SchikoMemberModel = $this->framework->createInstance('Memo\SchikoBundle\Model\SchikoMemberModel');
        $oMember = $SchikoMemberModel->findByGroup(2);
        $spreadsheet = new Spreadsheet();

        //Add Logo
        $drawing = new HeaderFooterDrawing();
        $drawing->setName('Swissfaustball Logo');
//        $drawing->setPath('bundles/schiko/vorlagen/logo.png');
        $drawing->setPath(dirname(dirname(__FILE__)).'/Resources/public/vorlagen/logo.jpg');
//        $drawing->setHeight(74);
//        $drawing->setCoordinates('A1');
//        $drawing->setResizeProportional(true);
//        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        $spreadsheet->getActiveSheet()->getHeaderFooter()->addImage($drawing, HeaderFooter::IMAGE_HEADER_LEFT);
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddHeader('&C&G');
        $spreadsheet->getActiveSheet()->getHeaderFooter()->setOddFooter('&Ladressverzeichnis-schiedsrichter&Cwww.swissfaustball.ch&R&D');

        //Settings
        $sheet       = $spreadsheet->getActiveSheet();
        $sheet->getPageSetup()->setFitToPage(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        $sheet->getPageSetup()->setOrientation(PageSetup::ORIENTATION_LANDSCAPE);

        $styleCellArray = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => 'FF000000')
                ],
            ]
        ];

        //Headline
        $iLineNo = 6;
        foreach($oMember as $key => $val) {

            $sheet->mergeCells("B".$iLineNo.":N".$iLineNo);
            $sheet->mergeCells("O".$iLineNo.":P".$iLineNo);
            $sheet->getCell('B'.$iLineNo)->setValue('Adressverzeichnis Schiedsrichter'); //Titel
            $sheet->getCell('O'.$iLineNo)->setValue('Schiedsrichterkommission SCHIKO'); //Titel
            $sheet->getStyle('B'.$iLineNo.':N'.$iLineNo)->getAlignment()->setHorizontal('center');
            $sheet->getStyle('O'.$iLineNo.':P'.$iLineNo)->getAlignment()->setHorizontal('right');
            $sheet->getStyle('B'.$iLineNo.':P'.$iLineNo)->getFont()->setSize(24);
            $sheet->getStyle('B'.$iLineNo.':P'.$iLineNo)->getFont()->setBold(true);
            $iLineNo = $iLineNo+2;

            //Headline
            $sheet->getCell(Coordinate::stringFromColumnIndex(1) . $iLineNo)->setValue(empty($val['category_label'])?'':$val['category_label']);
//            $sheet->getCellByColumnAndRow(1,$iLineNo)->setValue(empty($val['category_label'])?'':$val['category_label']); //Titel
            $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setSize(18);
            $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setBold(true);
            $iLineNo++;

            $col = 1;
            foreach($this->aColumnHeaders as $key => $column) {
                $sheet->getCell(Coordinate::stringFromColumnIndex($col) . $iLineNo)->setValue($column);
//                $sheet->getCellByColumnAndRow($col,$iLineNo)->setValue($column); //Titel
                $col++;
            }
            //Set Bold
            $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setSize(16);
            $sheet->getStyle('A'.$iLineNo.':P'.$iLineNo)->getFont()->setBold(true);
            $sheet->getStyle('A'.($iLineNo-1).':P'.$iLineNo)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setRGB('EFEFEF');
            $iLineNo++;

            for($x=0; $x < count($val['data']);$x++) {
                $i = 1;
                foreach ($this->aColumnHeaders as $hKey => $hVal) {
                    $data = '';
                    if($hKey === 'dateOfBirth') {
                        if(is_numeric($val['data'][$x]->$hKey)) {
                            if(strtotime(date('Y-m-d H:i:s',$val['data'][$x]->$hKey)) == $val['data'][$x]->$hKey && !empty($val['data'][$x]->$hKey) && $val['data'][$x]->$hKey !== 0) {
                                $data = date("d. m. Y", $val['data'][$x]->$hKey);
                            }else{
                                $data = '';
                            }
                        }elseif(!empty($val['data'][$x]->$hKey)){
                            $data = date("d. m. Y",strtotime($val['data'][$x]->$hKey));
                        }
                    }else{
                        $data = $val['data'][$x]->$hKey;
                    }
                    $sheet->getCellByColumnAndRow($i, $iLineNo)->setValue($data); //Titel
                    $i++;
                }

                //Style Row
                //Set Auto Height
                $sheet->getRowDimension($iLineNo)->setRowHeight(-1);
                foreach ($sheet->getColumnIterator() as $column) {
                    $sheet->getStyle($column->getColumnIndex().$iLineNo)->applyFromArray($styleCellArray);
                    //Break if last Column enter
                    if($column->getColumnIndex() == "P")
                    {
                        break;
                    }
                }
                $iLineNo++;
            }
            $spreadsheet->getActiveSheet()->setBreak('A'.$iLineNo, Worksheet::BREAK_ROW);
            $iLineNo = $iLineNo+2;
        }
        $sheet->getColumnDimension('P')->setAutoSize(TRUE);
        //Set Autowidth
        /** @var PHPExcel_Cell $cell */
        foreach( $sheet->getColumnIterator() as $column ) {
            $sheet->getColumnDimension($column->getColumnIndex())->setAutoSize(TRUE);
        }

        //Get Filename and output to browser
        $fileName = date("Y-m-d_")."Adressverzeichnis-Schiedsrichter.xls";
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');


        \Contao\Controller::redirect('/contao?do=competitions&table=tl_competition_request');


    }


    /**
     * @param bool $blnSavePdf
     * @return Response
     */
    public function exportDirectoryPDF(bool $blnSavePdf = false): Response {

        //Get Data
        $objSchikoModel	= $this->framework->createInstance('Memo\SchikoBundle\Model\SchikoMemberModel');
        $oMember = $objSchikoModel->findByGroup(2);
        $template = new \FrontendTemplate('schiko_directory_pdf_template');
        $template->data = $oMember;
        $template->basUrl = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
        $template->aColumnHeaders = $this->aColumnHeaders;

        $options = new Options();
        $options->set('isRemoteEnabled',true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($template->parse());
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'Adressverzeichnis-Schiedsrichter.pdf'
        );
        return new Response($dompdf->output(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => $disposition
        ));
    }


    /**
     * Export Aufgebot und Spesenabrechnung pro Runde als PDF
     * @param bool $blnSavePdf
     * @return Response
     */
    public function exportOfferPDF(int $roundID, string $prmSavePdfFile = null) {

        if(empty($roundID)) {
            return new Response();
        }

        //Get Data
        $sModel = new SchikoSurveyModel();
        $oData = $sModel->findByRound($roundID);
        $oCategory = $oData->getRelated('f_category');
        $oSeason = $oCategory->getRelated('f_season');

        $template = new \FrontendTemplate('schiko_offer_pdf_template');

        if(null !== $this->request->getCurrentRequest()) {
            $template->basUrl = $this->request->getCurrentRequest()->getSchemeAndHttpHost();
        }else {
            $template->basUrl = 'https://www.swissfaustball.ch';
        }

        $template->data = $oData;
        $template->season = $oSeason;

        $options = new Options();
        $options->set('isRemoteEnabled',true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($template->parse());
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        if(null === $prmSavePdfFile) {
            $disposition = HeaderUtils::makeDisposition(
                HeaderUtils::DISPOSITION_ATTACHMENT,
                'Aufgebot.pdf'
            );
            return new Response($dompdf->output(), 200, array(
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => $disposition
                )
            );
        }else{
            file_put_contents($prmSavePdfFile,$dompdf->output());
            return true;
        }
    }

    /**
     * Einsatzplan pro Saison als PDF
     * @param bool $blnSavePdf
     * @return Response
     */
    public function exportEntryPDF(int $season, string $prmSavePdfFile = null): Response {

        $template = new \FrontendTemplate('schiko_entry_pdf_template');
        //Get Data
        if(null !== $season) {
            $oSeason     = SfSeasonModel::findByPk($season);
            $oCategories = SfCategoriesModel::findBy(['tl_sf_categories.f_season=?','tl_sf_categories.active=?'],[$season,1],['order'=>' FIELD(tl_sf_categories.f_league,3,6,5,4,1,2), tl_sf_categories.category']);
            $aCategories = [];

            if(null === $oCategories) {
                return  new Response();
            }
            foreach($oCategories as $key => $oCat) {
                $oRounds = SfRoundsModel::findBy(['tl_sf_rounds.f_category=?'],[$oCat->id],['order'=>'date, time']);
                if(null !== $oRounds) {
                    $aCategories[$key] = $oCat;
                    $aCategories[$key]->rounds = $oRounds->fetchAll();
                }
            }

            $oSurvey = SchikoSurveyModel::findBy(['tl_schiko_survey.season=?','tl_schiko_survey.status=?'],[$oSeason->id,'booked'],['order'=>"FIELD(function ,'manager','referee','linesman'),season, category, round"]);
            $aSurvey = [];
            $aBooked = [];
            if(!empty($oSurvey)) {
                foreach ($oSurvey as $key => $val) {
                    if($val->status == 'booked') {
                        $aBooked[$val->round] = empty($aBooked[$val->round])? 1 : $aBooked[$val->round]+1;
                    }
                    $aSurvey[$val->round][] = $val;
                }
            }

            \Contao\System::loadLanguageFile('tl_schiko_survey', 'de');
            \Controller::loadDataContainer('tl_member');
            $optionLabelsSchikoTags = $GLOBALS['TL_DCA']['tl_member']['fields']['schiko_tags']['options'];

            $template->season     = $oSeason;
            $template->optionLabelsSchikoTags = $optionLabelsSchikoTags;
            $template->booked     = $aBooked;
            $template->categories = $aCategories;
            $template->aSurvey    = $aSurvey;
            $template->aFunctionOptions    = $GLOBALS['TL_LANG']['tl_schiko_survey']['function_options'];
        }
        $template->basUrl = $this->request->getCurrentRequest()->getSchemeAndHttpHost();

        $options = new Options();
        $options->set('isRemoteEnabled',true);
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($template->parse());
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();

        $font = $dompdf->getFontMetrics()->get_font("helvetica");
        $dompdf->getCanvas()->page_text(770, 570, "Seite: {PAGE_NUM} / {PAGE_COUNT}", $font, 8, array(0,0,0));
        $dompdf->getCanvas()->page_text(30, 570, "Version: ".date("d.m.Y"), $font, 8, array(0,0,0));



        // Output the generated PDF to Browser
        if(null === $prmSavePdfFile) {
            $disposition = HeaderUtils::makeDisposition(
                HeaderUtils::DISPOSITION_ATTACHMENT,
                'Einsatzplan.pdf'
            );

            return new Response($dompdf->output(),200,[
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => $disposition
            ]);
        }else{
            file_put_contents($prmSavePdfFile,$dompdf->output());
            return new Response();
        }
    }

    /**
     * @param $prmSeasonId
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function _getClubsBySeason($prmSeasonId=NULL): array
    {
        $arrReturn = [];

        if(null === $prmSeasonId)
        {
            return $arrReturn;
        }

        $strSQL = "SELECT c.id as 'club_id', c.`name` as 'club_name', tl_sf_rounds.id as round_id,tl_sf_rounds.date,tl_sf_rounds.time, tl_sf_rounds.org_place, tl_sf_rounds.org_address, tl_sf_rounds.org_zipcode, tl_sf_categories.category, s.season, IF(s.type = 1,'Feld','Halle') as type_name,s.type, s.schiko_driving_expenses
                    FROM tl_sf_rounds
                        INNER JOIN tl_sf_categories ON tl_sf_rounds.f_category = tl_sf_categories.id
	                    INNER JOIN tl_mod_club AS c	ON c.id = tl_sf_rounds.schiko_organizer
	                    INNER JOIN	tl_sf_season as s ON tl_sf_categories.f_season = s.id
                    where s.id = $prmSeasonId

                    ORDER BY c.sortname, tl_sf_rounds.date";

        $arrResult = $this->db->executeQuery($strSQL)->fetchAllAssociative();

        foreach($arrResult as $key => $val)
        {
            $arrReturn[$val['club_id']][] = $val;
        }

        return $arrReturn;
    }

    /**
     * @param $prmSeasonId
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function _getCommissionBillingData($prmSeasonId=NULL): array
    {
        $arrReturn = [];

        if(null === $prmSeasonId)
        {
            return $arrReturn;
        }

        $strSQL = "SELECT c.id AS category_id, r.id AS round_id, schiko.id AS schiko_id,schiko.name AS schiko_name,r.date, r.time, r.org_place, r.org_address, r.org_zipcode, c.category, s.season, IF(s.type = 1,'Feld','Halle') AS type_name, s.type, s.schiko_driving_expenses, schiko.`name` AS schiko_name,schiko.`shortname` AS schiko_shortname
                    FROM
                        tl_sf_rounds r
                        INNER JOIN tl_sf_categories as c	ON 	r.f_category = c.id
                        INNER JOIN tl_sf_season AS s	ON c.f_season = s.id
                        INNER JOIN tl_schiko_commission schiko	ON 		c.schiko_commission = schiko.id
                    WHERE
                        s.id = $prmSeasonId
                        and schiko.published = 1
                    ORDER BY
                        schiko.name, r.date ASC";

        $arrResult = $this->db->executeQuery($strSQL)->fetchAllAssociative();

        foreach($arrResult as $key => $val)
        {
            $arrReturn[$val['schiko_id']][] = $val;
        }
        return $arrReturn;
    }

    /**
     * @param int $user
     * @return false|Response
     */
    public function exportRefereeIdCard(int $user): Response
    {
        if(empty($user))
        {
            return new Response();
        }

        $oMember = MemberModel::findByPk($user);
        if(empty($oMember))
        {
            return new Response();
        }


        $fontSize = 10;
        $cellHeight = 6;
        $cellSpace = 7;

        $pdf = new \FPDF();
        $pdf->AddPage('L');
        $pdf->SetFont('Arial','B',$fontSize);
        $pdf->SetFillColor(230,230,230);
        $pdf->SetDrawColor(100,100,100);

        $pdf->Line(8,10,95, 10);
        $pdf->Line(8,65,95, 65);
        $pdf->Line(10,8,10, 67);
        $pdf->Line(95,8,95, 67);

        $pdf->Line(95,10,182, 10);
        $pdf->Line(95,65,182, 65);
        $pdf->Line(180,8,180, 67);

        //Content

        $pdf->Image('https://www.swissfaustball.ch/files/template/img/logo.png',80,12,12,0);
        if(empty($oMember->singleSRC)) {
            $pdf->SetXY(13,28);
            $pdf->Cell(28,34,'',1,0,'',1);
        }else {
            $image = FilesModel::findByUuid($oMember->singleSRC);
            $pdf->Image($image->path, 13, 28, 0, 34);
            $pdf->SetXY(13,28);
            $pdf->Cell(28,34,'',1,0,'',0);
        }

        $pdf->SetY(28);
        $pdf->SetX(45);
        $pdf->Cell(23,$cellHeight,'Name:',0,0,'',1);
        $pdf->SetFont('Arial','',$fontSize);
        $pdf->Cell(25,$cellHeight,utf8_decode($oMember->lastname),0,0,'',1);

        $pdf->Ln($cellSpace);
        $pdf->SetX(45);
        $pdf->SetFont('Arial','B',$fontSize);
        $pdf->Cell(23,$cellHeight,'Vorname:',0,0,'',1);
        $pdf->SetFont('Arial','',$fontSize);
        $pdf->Cell(25,$cellHeight,utf8_decode($oMember->firstname),0,0,'',1);

        $pdf->Ln($cellSpace);
        $pdf->SetX(45);
        $pdf->SetFont('Arial','B',$fontSize);
        $pdf->Cell(23,$cellHeight,'Geb. Datum:',0,0,'',1);
        $pdf->SetFont('Arial','',$fontSize);
        $pdf->Cell(25,$cellHeight,\Date::parse('d. m. y',$oMember->dateOfBirth),0,0,'',1);

        $pdf->Ln($cellSpace);
        $pdf->SetX(45);
        $pdf->SetFont('Arial','B',$fontSize);
        $pdf->Cell(23,$cellHeight,'Brevet-Jahr:',0,0,'',1);
        $pdf->SetFont('Arial','',$fontSize);
        $pdf->Cell(25,$cellHeight,$oMember->brevet_year,0,0,'',1);

        $pdf->Ln($cellSpace);
        $pdf->SetX(45);
        $pdf->SetFont('Arial','B',$fontSize);
        $pdf->Cell(23,$cellHeight,'Kategorie:',0,0,'',1);
        $pdf->SetFont('Arial','',$fontSize);
        $pdf->Cell(25,$cellHeight,strtoupper(implode(",",\StringUtil::deserialize($oMember->schiko_tags))),0,0,'',1);

        $arrAddress = ['Adresse' => $oMember->street,'PLZ'=>$oMember->postal,'Ort'=>$oMember->city,'Tel_P'=>$oMember->phone,'Mobil'=>$oMember->mobile,'E-Mail'=>$oMember->email,'Verein'=>$oMember->club];

        $pdf->SetXY(100, 7);
        foreach($arrAddress as $key => $val ) {
            $pdf->Ln($cellSpace);
            $pdf->SetX(100);
            $pdf->SetFont('Arial', 'B', $fontSize);
            $pdf->Cell(17, $cellHeight, str_replace("_",". ",$key).':', 0, 0, '', 1);
            $pdf->SetFont('Arial', '', $fontSize);
            $pdf->Cell(60, $cellHeight, utf8_decode($val), 0, 0, '', 1);
        }

        // Output PDF
        $fName = filter_var($oMember->lastname . '_id_card.pdf', FILTER_SANITIZE_URL);
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT, $fName
        );
        return new Response($pdf->Output(), 200, array(
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => $disposition
        ));
    }


    public function exportXlsInsets($season_id)
    {
        if(empty($season_id))
        {
            \Contao\Controller::redirect('/contao?do=tl_sf_season');
        }

        $oSeason = SfSeasonModel::findByPk($season_id);
        $oInsets = SchikoSurveyModel::findBy(['season=?','status=?'],[$season_id,'booked'],['order'=>'category, tstamp']);
        $spreadsheet = new Spreadsheet();

        //order insets
        $arrInsets = [];
        foreach($oInsets as $key => $inset) {
            $tmpInset = $inset;
            if(empty($arrInsets[$inset->referee]['email'])) {
                $oReferee = $inset->getRelated('referee');
                $arrInsets[$oReferee->id]['firstname'] = $oReferee->firstname;
                $arrInsets[$oReferee->id]['lastname'] = $oReferee->lastname;
                $arrInsets[$oReferee->id]['email'] = $oReferee->email;
                $arrInsets[$oReferee->id]['count_for_club'] = $oReferee->count_for_club;
                $arrInsets[$oReferee->id]['tags'] = unserialize($oReferee->schiko_tags);
            }
            $oRound     = SfRoundsModel::findById($inset->round);
            $oCategorie = SfCategoriesModel::findById($inset->category);
            if(!empty($oRound)) {
                $tmpInset->location = $oRound->location;
                $tmpInset->date = $oRound->date;
                $tmpInset->time = $oRound->time;
            }else{
                $tmpInset->date = $tmpInset->time = $tmpInset->location = '';
            }
            $tmpInset->category_label = $oCategorie->category;
            $arrInsets[$inset->referee]['inset'][] = $tmpInset;
        }

        $template = new \FrontendTemplate('schiko_export_insets.xls');
        $template->season = $oSeason;
        $template->insets = $arrInsets;
        $template->basUrl = $this->request->getCurrentRequest()->getSchemeAndHttpHost();

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $reader->loadFromString($template->parse());

        //Get Filename and output to browser
        $fileName = date("Y-m-d_")."SchiedsrichterEinsaetze_".$oSeason->season.".xls";
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xls");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'. urlencode($fileName).'"');
        $writer->save('php://output');
    }

}
