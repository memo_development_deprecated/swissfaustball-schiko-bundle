<?php

namespace Memo\SchikoBundle\Service;


use Geocoder\Query\GeocodeQuery;
use Geocoder\Provider\OpenRouteService\OpenRouteService;
use \Geocoder\StatefulGeocoder;
use Geocoder\Query\ReverseQuery;
use Symfony\Component\HttpClient\HttpClient;

class GeocoderService
{

    private $googleMapsApiKey;
    private $openRouteKey;

    public function __construct()
    {
        $this->googleMapsApiKey = \Config::get('schiko_google_maps_api_key');
        $this->openRouteKey = \Config::get('schiko_openroute_key');
    }

    /**
     * @param null $prmAddress
     * @return false|void
     * @throws \Geocoder\Exception\Exception
     */
    public function geocodeAddress($prmAddress = NULL)
    {
        $result = NULL;
        if(null == $this->openRouteKey OR empty($prmAddress)) {
            return $result;
        }
        $httpClient = new \GuzzleHttp\Client();
        $provider   = new OpenRouteService($httpClient,$this->openRouteKey);
        $geocoder   = new StatefulGeocoder($provider, 'de');

        $result = $geocoder->geocodeQuery(GeocodeQuery::create($prmAddress));
        return $result;
    }

    /**
     * @param null $latitude
     * @param null $longitude
     * @return false|\Geocoder\Collection
     * @throws \Geocoder\Exception\Exception
     */
    public function getAddressByCoordinates($latitude = NULL, $longitude = NULL) {
        if(empty($latitude) OR empty($longitude) OR empty($this->googleMapsApiKey)) {
            return false;
        }
        $result = NULL;
        $httpClient = new \GuzzleHttp\Client();
        $provider   = new OpenRouteService($httpClient,$this->openRouteKey);
        $geocoder   = new StatefulGeocoder($provider, 'de');
        $result = $geocoder->reverseQuery(ReverseQuery::fromCoordinates($latitude, $longitude));
        return $result;
    }

    /**
     * @param $start
     * @param $end
     * @param $profile
     * @return array
     * @throws \Safe\Exceptions\JsonException
     */
    public function getDistanceBetween($start,$end,$profile = 'driving-car'):array {
        $aReturn = [];
        if(empty($start) OR empty($end)){
            return $aReturn;
        }

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://api.openrouteservice.org/v2/matrix/".$profile);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
                "locations":[
                    ['.$start.'],
                    ['.$end.']
                ],
                "metrics":["distance","duration"]
            }');

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Accept: application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8",
            "Authorization: ".$this->openRouteKey,
            "Content-Type: application/json; charset=utf-8"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        if(null !== $response) {
            $aResponse = \Safe\json_decode($response);
            $duration  = empty($aResponse->durations[1][0])? 0 : $aResponse->durations[1][0];
            $h  = floor(($duration/3600));
            $m  = floor(($duration-($h*3600))/60);
            $aReturn['duration'] = ['formated'=> "$h:$m",'seconds'=>$duration];
            $aReturn['distance'] = empty($aResponse->distances[1][0])? 0 : $aResponse->distances[1][0];
        }
        return $aReturn;
    }



}
