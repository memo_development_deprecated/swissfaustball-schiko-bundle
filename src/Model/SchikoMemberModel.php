<?php

namespace Memo\SchikoBundle\Model;

use Contao\MemberModel;
use Model\Collection;

class SchikoMemberModel extends \Model {

    /**
     * @var string
     */
    protected static $strTable = 'tl_member';


    public function findByGroup(int $groupID): array {
        if(empty($groupID)){
            return [];
        }
        $oMember = MemberModel::findBy(['groups like ?'],['%"'.$groupID.'"%'],['order'=>'schiko_tags,lastname,firstname']);

        //Assign DCA Values
        \Controller::loadDataContainer('tl_member');
        if(null !== $GLOBALS['TL_DCA']['tl_member']['fields']['schiko_tags']['options']) {
            $tagLabel = $GLOBALS['TL_DCA']['tl_member']['fields']['schiko_tags']['options'];
            foreach($GLOBALS['TL_DCA']['tl_member']['fields']['schiko_tags']['options'] as $key => $data) {
                $aReturn[$key] = [];
            }
        }else {
            //Default DCA Values
            $aReturn = ['i'=>[],'a'=>[],'b'=>[],'z'=>[],'n'=>[]];
            $tagLabel = ['i'=>[],'a'=>[],'b'=>[],'z'=>[],'n'=>[]];
        }

        foreach($oMember as $key => $val) {
            $aKeys = unserialize($val->schiko_tags);
            if($aKeys == false) {
                $aKeys[] = $val->schiko_tags;
            }

            for($i=0; $i < count($aKeys);$i++) {
                $aReturn[$aKeys[$i]]['data'][] = $val;
                if(empty($aReturn[$aKeys[$i]]['category_label']) && !empty($tagLabel[$aKeys[$i]])) {
                    $aReturn[$aKeys[$i]]['category_label'] = $tagLabel[$aKeys[$i]];
                }
            }
        }

        //remove empty keys
        $aReturn = array_filter($aReturn, function($data) {
           if(!empty($data)) {
               return true;
           }
           return false;
        });
        return $aReturn;
    }

}
