<?php

namespace Memo\SchikoBundle\Model;


use Contao\Image\Exception\ExceptionInterface;
use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Memo\SchikoBundle\Service\GeocoderService;
use NotificationCenter\Model\Notification;
use Doctrine\DBAL\Exception;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Model\Collection;

class SchikoSurveyModel extends \Model
{

    /**
     * @var string
     */
    protected static $strTable = 'tl_schiko_survey';

    /**
     * @const array
     */
    protected const RegisterStatus = ['possible', 'booked', 'cancel'];


    /**
     * @param $prmStatus
     * @param $blnSendMail
     * @return bool
     */
    public function setRegisterStatus(string $prmStatus = null, bool $blnSendMail = false): bool
    {
        //empty status
        if ($prmStatus === null) {
            return false;
        }

        //wrong status
        if (!in_array($prmStatus, self::RegisterStatus)) {
            return false;
        }
        try {
            $this->status = $prmStatus;
            $this->save();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
            return false;
        }

        //Prepare Notification
        $oSeason = SfSeasonModel::findByPk($this->season);

        //Notification nicht gewünscht oder nicht gesetzt
        if (null !== $oSeason and $oSeason->schiko_send_decline_mail == 1 and !empty($oSeason->schiko_decline_notification)) {

            //Prepare Notification
            $aToken = [];
            $objNotification = Notification::findByPk($oSeason->schiko_decline_notification);
            $oRound = SfRoundsModel::findByPk($this->round);

            if (null === $objNotification) {
                throw new Exception('can\'t find notification E-Mail (schiko bundle setRegisterStatus)');
                return false;
            }

            $aToken['form_round_name'] = date('d. m. Y', $oRound->date) . " / " . $oRound->location;
            $aToken['form_category']   = $oRound->getRelated('f_category')->category;
            $aToken['form_round_date'] = date('d. m. Y', $oRound->date);
            $aToken['form_recipient']  = $this->getRelated('referee')->email;
            $aToken['form_recipient_firstname'] = $this->getRelated('referee')->firstname;
            $aToken['form_recipient_name'] = $this->getRelated('referee')->lastname;
            $aToken['form_status'] = $prmStatus;

            try {
                $objNotification->send($aToken, 'de');
            }catch (Exception $e) {
                throw new Exception($e->getMessage());
                return false;
            }
            return true;
        }

        return true;
    }

    /**
     * @param int $prmRoundID
     * @return Collection
     */
    public function findByRound(int $prmRoundID)
    {
        if (empty($prmRoundID)) {
            return new Collection();
        }

        $oReturn = [];
        $oSchiko = (object)[];

        //Round Infos
        $oReturn = SfRoundsModel::findByPk($prmRoundID);
        $oCategory = $oReturn->getRelated('f_category');
        $aRate = \StringUtil::deserialize($oCategory->schiko_rate);
        if (null !== $aRate && is_array($aRate)) {
            $aRate = $aRate[0];
        } else {
            $aRate = ['label' => ''];
        }
        if (isset($aRate['sr_food'])) {
            $aRate['label'] = $aRate['sr_food'] == 'snack' ? 'Zwischenverpflegung' : 'Volle Verpflegung';
        }
        $oSchiko->rate = $aRate;
        $oSchiko->referee_count = $oCategory->schiko_referee_count;
        $oSchiko->commission = $oCategory->schiko_commission;
        $oReturn->schiko = $oSchiko;

        $options = ['order' => 'season, round DESC '];
        $referee = self::findBy(['round=?', 'status=?'], [$prmRoundID, 'booked'], $options);
        if (null !== $referee) {
            $oReturn->referee = $referee;
        }
        return $oReturn;
    }

    /**
     * @param int $prmRoundID
     * @return bool
     * @throws \Safe\Exceptions\JsonException
     */
    static public function calcHonorByRound(int $prmRoundID, int $prmSurveyID = null,$blnForceRecalc=false): bool {
        if (empty($prmRoundID)) {
            return false;
        }

        $aRates     = [];
        $oRound     = SfRoundsModel::findByPk($prmRoundID);
        $oCategory  = $oRound->getRelated('f_category');
        $oSeason    = $oCategory->getRelated('f_season');

        if ($oCategory->schiko_rate) {
            $aRates = \StringUtil::deserialize($oCategory->schiko_rate);
            if(isset($aRates[0]['sr_value']) AND $aRates[0]['sr_value'] != '')
            {
                $fee = $aRates[0]['sr_value'];
            }
        }

        $oSurveyMembers = SchikoSurveyModel::findBy(['round=?','category=?'],[$prmRoundID,$oCategory->id]);

        if(empty($oSurveyMembers)){
            return false;
        }

        //Loop thru all referees
        foreach($oSurveyMembers as $key => $oSurvey)
        {
            if(null != $prmSurveyID && $prmSurveyID !== $oSurvey->id) {
                continue;
            }

            $oReferee = $oSurvey->getRelated('referee');

            if($blnForceRecalc || (null === $oSurvey->travel_cost && $oSurvey->travel_cost !== 0)) {
                $geoCoderService = new GeocoderService();
                if (null !== $oRound && !empty($oRound->org_lng) && !empty($oRound->org_lat) && !empty($oReferee->coordinates)) {

                    $aRefereeCoordinates = explode(",", $oReferee->coordinates);
                    $start = sprintf('%s,%s', trim($aRefereeCoordinates[1]), trim($aRefereeCoordinates[0]));
                    $end = sprintf('%s,%s', trim($oRound->org_lng), trim($oRound->org_lat));

                    if(empty($oSurvey->travel_distance) || $blnForceRecalc) {
                        $distance = $geoCoderService->getDistanceBetween($start, $end);
                        $distance['distance'] = 2*$distance['distance'];
                    }else{
                        $distance['distance'] = ($oSurvey->travel_distance*1000);
                    }

                    if (null !== $oSeason && !empty($distance['distance'])) {
                        $drivingTax = empty($oSeason->schiko_driving_expenses) ? floatval(0.5) : floatval($oSeason->schiko_driving_expenses);
                        $travel_distance = round(floatval($distance['distance'] / 1000)); //Hin und zurück, beide Wege
                        $directionFee = round(($drivingTax * $travel_distance), 2);
                        $oSurvey->travel_cost = ceil($directionFee);
                        $oSurvey->travel_distance = $travel_distance;
                    }
                }
            }

            $total   = intval($fee) + intval($oSurvey->travel_cost) + intval($oSurvey->porto) + intval($oSurvey->bonus);
            $oSurvey->total = $total;
            $oSurvey->fee   = $fee;
            $oSurvey->save();
        }
        return true;
    }

}

?>
