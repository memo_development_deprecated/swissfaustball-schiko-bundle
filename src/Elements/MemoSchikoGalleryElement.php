<?php


/**
 * Contao Open Source CMS
 *
 * Copyright (c) Media Motion AG
 *
 * @package   SelectLineBundle
 * @author	Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\SchikoBundle\Elements;

use Contao\CoreBundle\Image\Studio\Studio;
use Memo\SchikoBundle\Model\SchikoMemberModel;

class MemoSchikoGalleryElement	 extends \ContentElement
{

    protected $strTemplate = 'ce_memo_schiko_gallery';

    /**
     * Displays a wildcard in the back end.
     * @return string
     */
    public function generate()
    {

        if (TL_MODE == 'BE') {

//            $template = new \BackendTemplate('be_wildcard');
//            $template->wildcard = sprintf('<%s>%s</%s>',$this->hl,$this->headline,$this->hl).'### MEMO Event List:'.$this->itemCategories.' ###';
//            return $template->parse();

        }

        return parent::generate();
    }


    protected function compile()
    {
        // TODO: Implement compile() method.
        $aMembers = unserialize($this->MemoSchikoGalleryMembers);


        if(!empty($aMembers)) {
            $oMemberData = \MemberModel::findMultipleByIds($aMembers);
        }

        $this->Template->Members       = $oMemberData;
        $this->Template->sCssID       = trim($this->cssID[0]);
        $this->Template->sCssClass    = $this->cssID[1];
        $GLOBALS['TL_CSS'][]        = '/bundles/schiko/css/schiko_default.css|static';
    }
}
