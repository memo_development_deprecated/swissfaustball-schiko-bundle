<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\SchikoBundle\Module;

use Contao\FrontendUser;
use Contao\Module;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use Memo\SchikoBundle\Service\GeocoderService;


class SchikoSurveyModule extends Module
{

    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_schiko_survey_register';


    /**
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE')
        {
            $objTemplate = new \Contao\BackendTemplate('be_wildcard');
            $objTemplate->wildcard = $strWildcard = 'An dieser Stelle erscheinen im Frontend die Einsatz Registrierung - Formular.';
            $objTemplate->title = $this->headline;
            $objTemplate->id = $this->id;
            $objTemplate->link = $this->name;

            return $objTemplate->parse();
        }
        return parent::generate();
    }

    /**
     * Compile Function
     */
    protected function compile()
    {
        if (TL_MODE === 'FE')
        {
            if(!empty($this->season)) {
                $user        = FrontendUser::getInstance();
                $oSeason     = SfSeasonModel::findByPk($this->season);
                $oCategories = SfCategoriesModel::findBy(['tl_sf_categories.f_season=?','tl_sf_categories.active=?'],[$this->season,1],['order'=>' FIELD(tl_sf_categories.f_league,3,6,5,4,1,2), tl_sf_categories.category']);
                $aRoundsByDate = [];

                foreach ($oCategories as $key => $oCat) {
                    if(isset($this->bln_hide_played_rounds) AND $this->bln_hide_played_rounds == 1)
                    {
                        $oRounds = SfRoundsModel::findBy(['tl_sf_rounds.f_category=?','tl_sf_rounds.date > ?'], [$oCat->id,mktime(0,0,0,date('m',time()),date('d',time()),date('Y',time()))], ['order' => 'date, time']);
                    }else {
                        $oRounds = SfRoundsModel::findBy(['tl_sf_rounds.f_category=?'], [$oCat->id], ['order' => 'date, time']);
                    }
                    if (null !== $oRounds) {
                        $oCategories[$key]->rounds = $oRounds->fetchAll();

                        foreach($oRounds as $rKey => $round)
                        {
                            $aRoundsByDate[$round->date][$round->id] = $round;
                            $aRoundsByDate[$round->date][$round->id]->categorie = $oCat;
                        }
                    } else {
                        $oCategories[$key]->rounds = [];
                    }
                }
                //order by day
                ksort($aRoundsByDate);

                $oSurvey = SchikoSurveyModel::findBy(['season=?'], [$oSeason->id], ['order' => 'season, category, round']);
                $aSurvey = [];
                $aBooked = [];
                if (!empty($oSurvey)) {
                    foreach ($oSurvey as $key => $val) {
                        if ($val->status == 'booked') {
                            $aBooked[$val->round] = empty($aBooked[$val->round]) ? 1 : $aBooked[$val->round] + 1;
                        }
                        if ($val->referee == $user->id) {
                            $aSurvey[$val->round]['status'] = $val->status;
                            $aSurvey[$val->round]['id'] = $val->id;
                        }
                    }
                }


                $this->Template->season     = $oSeason;
                $this->Template->booked     = $aBooked;
                $this->Template->categories = $oCategories;
                $this->Template->aRoundsByDate = $aRoundsByDate;
                $this->Template->aSurvey    = $aSurvey;

                $GLOBALS['TL_CSS'][]        = '/bundles/schiko/css/schiko_default.css|static';
                $GLOBALS['TL_JAVASCRIPT'][] = '/bundles/schiko/js/schiko_default.js';
            }

        }
    }




}
