'use strict';

window.addEventListener('DOMContentLoaded', (event) => {

    window.jQuery = $;
    window.$ = $;

    function showNotification(prmType)
    {
        prmType = undefined ? 'success' : prmType;
        const note = document.createElement('div');
        note.classList.add('toast',prmType);
        const cont = document.createTextNode("Eintrag erfolgreich gespeichert!");
        note.appendChild(cont);
        document.getElementById('toast-wrapper').appendChild(note);
        setTimeout(function(){note.classList.add('toast_show');},200);
        setTimeout(function(){
            note.classList.remove('toast_show');
            setTimeout(function(){note.remove();},1000);
        },5000)
    }

    let buttons = $('button.btn--success');
    buttons.each(function(idx, el){
        $(el).on('click',function(e){
            e.preventDefault();
            var wrapper = $(this).parent();
            this.classList.add('btn--hide');
            $(this).parent().addClass('checkmark--reserved');
            $(".btn--cancel",$(this).parent()).toggleClass('btn--hide');
            showNotification('success');
            let rData = $(this).parent().data();
            $.post('/schiko/register',rData,function(data) {
                if(data.success) {
                    $(wrapper).data('survey-id',data.surveyId);
                }
            });

        });
    });

    let cancelButtons = $('button.btn--cancel');
    cancelButtons.each(function(idx, el){
        $(el).on('click',function(e){
            e.preventDefault();
            this.classList.add('btn--hide');
            $(this).parent().removeClass('checkmark--reserved');
            $(".btn--success",$(this).parent()).toggleClass('btn--hide');
            let rData = $(this).parent().data();
            $.post('/schiko/unregister',rData,function(data) {

            });
        });
    });
});
