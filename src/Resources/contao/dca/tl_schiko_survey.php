<?php



/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */




/**
 * Table tl_schiko_survey
 */

$GLOBALS['TL_DCA']['tl_schiko_survey'] = [
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'notCopyable'  => false,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
        'onsubmit_callback' =>  [['tl_schiko_survey','calcHonors']],
    ],
    'list' => [
        'sorting' => [
            'mode'      => 1,
            'fields'    =>  ['round asc','category DESC'],
            'panelLayout' => 'filter;sort,search,limit',
            'flag'      => 12,
        ],
        'label' => [
            'fields'=>[
                'season',
                'referee',
                'status'

            ],
            'format'         => '%s£%s£%s',
            'label_callback' => ['tl_schiko_survey','getColumnValues']
        ],
        'global_operations' => array
        (
            'all' => array
            (
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'decline' => [
                'href'					=> 'key=decline',
                'icon'					=> '/bundles/schiko/img/down.svg',
                'attributes'			=> 'onclick="Backend.getScrollOffset();"',
            ],
            'edit' => array
            (
                'href'					=> 'act=edit',
                'icon'					=> 'edit.gif',
                'attributes'			=> 'onclick="Backend.getScrollOffset();"',
            ),
            'delete' => array
            (
                'href'					=> 'act=delete',
                'icon'					=> 'delete.gif',
                'attributes'			=> 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'show' => array
            (
                'href'                => 'act=show',
                'icon'                => 'show.svg'
            )
        ),
    ],
    // Palettes
    'palettes' => array
    (
        '__selector__'                => array(''),
        'default'                     => '{survey_legend},season,category,round,referee,status,function,fee,porto,travel_cost,travel_distance,total,bonus,calcTravelDistance;
                                          {expert_legend:hide},cssID;
                                          '
    ),
    'fields' => [
        'id' => array(
            'label'                   => array('ID'),
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array(
            'sorting'   => true,
            'flag'      => 12,
            'inputType' => 'text',
            'sql'       => "int(10) unsigned NOT NULL default '0'"
        ),
        'season' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'foreignKey'    => 'tl_sf_season.season',
            'options_callback' => ['tl_schiko_survey','getSeasonOptions'],
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'disabled'=>true),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'category' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'foreignKey'    => 'tl_sf_categories.category',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'disabled'=>false),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'round' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'options_callback' => ['tl_schiko_survey','getRoundOptions'],
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'disabled'=>false),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'referee' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'options_callback'    => ['tl_schiko_survey','getMemberOptions'],
            'relation'      => ['load'=>'lazy','type'=>'hasOne', 'table'=>'tl_member','field'=>'id'],
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'disabled'=>false),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'status' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'reference'     => &$GLOBALS['TL_LANG']['tl_schiko_survey']['status_options'],
            'options'       => ['possible','booked','cancel'],
            'save_callback' => [['tl_schiko_survey','changeState']],
            'eval'          => array('chosen'=>false, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'notify_member' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true),
            'sql'           => "int(1) NOT NULL default 0"
        ],
        'notify_schiko' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true),
            'sql'           => "int(1) NOT NULL default 0"
        ],
        'function' => [
            'exclude'       => true,
            'filter'		=> true,
            'sorting'       => true,
            'inputType'     => 'select',
            'reference'     => &$GLOBALS['TL_LANG']['tl_schiko_survey']['function_options'],
            'options'       => ['manager','referee','linesman'],
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true),
            'save_callback' => [['tl_schiko_survey','addPorto']],
            'sql'           => "varchar(255) NOT NULL default ''"
        ],
        'fee' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>false, 'disabled'=>"true"),
            'sql'           => "int(5) NULL default 0"
        ],
        'travel_cost' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>false),
            'sql'           => "float(11,2) NULL default 0"
        ],
        'porto' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>false),
            'sql'           => "float(11,2) NULL default 0"
        ],
        'total' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>false,'disabled'=>'true'),
            'sql'           => "float(11,2) NULL default 0"
        ],
        'travel_distance' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>false,'disabled'=>'true'),
            'sql'           => "float(11,2) NULL default 0"
        ],
        'bonus' => [
            'exclude'       => true,
            'filter'		=> false,
            'sorting'       => false,
            'inputType'     => 'text',
            'eval'          => array('chosen'=>false, 'mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
            'sql'           => "varchar(255) NOT NULL default ''"
        ],

        'cssID' => array
        (
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('multiple'=>true, 'size'=>2, 'tl_class'=>'w50 clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
        'calcTravelDistance' => [
            'inputType'               => 'checkbox',
            'eval'                    => array('tl_class'=>'w50 mt12 clr','includeBlankOption' => false,'choosen'=>false,'mandatory'=>false),
            'sql'                       => "int(1) NOT NULL default 0"
        ]
    ]
];

use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use Memo\SchikoBundle\Service\GeocoderService;

class tl_schiko_survey extends Backend {

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Contao\BackendUser', 'User');
    }


    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getSeasonOptions(DataContainer $dc): array {
        $aReturn = [];

        $oSeason = SfSeasonModel::findAll(['order'=>'season desc']);
        if($oSeason) {
            foreach($oSeason as $key => $val) {
                $strType = $val->type == 0? 'Halle' : 'Feld';
                $aReturn[$val->id] = sprintf("%s (%s)",$val->season,$strType);
            }
        }
        return $aReturn;
    }

    /**
     * Get only Users from group ID = 2 :SCHIKO
     * @param DataContainer $dc
     * @return array
     */
    public function getMemberOptions(DataContainer $dc):array{
        $aReturn = [];
        $oMembers = MemberModel::findAll(['order'=>'lastname,firstname']);
        if(!empty($oMembers)) {
            foreach ($oMembers as $key => $val) {
                $usr_groups = unserialize($val->groups);
                if(is_array($usr_groups) && in_array(2,$usr_groups)) {
                    $aReturn[$val->id] = sprintf("%s %s", $val->lastname, $val->firstname);
                }
            }
        }
        return $aReturn;
    }

    /**
     * @param array $record
     * @param string $label
     * @param DataContainer $dc
     * @param array $aLabels
     * @return array
     */
    public function getColumnValues(array $record,string $label, DataContainer $dc, array $aLabels){

        $oSeason    = SfSeasonModel::findByPk($record['season']);
        $oRound     = SfRoundsModel::findByPk($record['round']);
        $oMember    = MemberModel::findByPk($record['referee']);

        if(!empty($oRound)) {
            $oCategory = $oRound->getRelated('f_category');
        }

        //Season
        $aLabels[0] = '<span class="col_saison">';
        //Category
        if(!empty($oCategory)) {
            $aLabels[0] .= $oCategory->category;
        }
        $aLabels[0]  .= '</span>';

        //Referee
        if(!empty($oMember)) {
            $aLabels[1] = sprintf('<span class="col_referee"><a href="contao?do=member&act=edit&id=%d" target="_blank" title="%s">%s %s</a></span>',$oMember->id,$oMember->email,$oMember->firstname, $oMember->lastname);
        }
        $status = $GLOBALS['TL_LANG']['tl_schiko_survey']['status_options'][$record['status']];
        $func   = empty($record['function'])? $status : $GLOBALS['TL_LANG']['tl_schiko_survey']['function_options'][$record['function']];
        $aLabels[2] = sprintf('<span class="col_status"><span class="%s">%s</span></span>',$record['status'],$func);
        unset($aLabels[3]);
        return $aLabels;
    }

    /**
     * @param array $data
     * @return false
     */
    public function changeState($state, $dc) {

        if(empty($state)) {
            return $state;
        }

        $record     = $dc->activeRecord;
        //Keine Änderung
        if($state == $record->status){
            return $state;
        }

        $recordID = $dc->id;
        if(!empty($recordID)){
            $oRecord = SchikoSurveyModel::findByPk($recordID);
            $oRecord->setRegisterStatus($state,1);
        }
        return $state;
    }

    /**
     * @param array $data
     * @return false
     */
    public function accept($dc) {
        $recordID = $dc->id;
        if(!empty($recordID)){
            $oRecord = SchikoSurveyModel::findByPk($recordID);
            $oRecord->setRegisterStatus('booked');
        }
        Controller::redirect('contao?do=survey');
    }

    /**
     * @param $data
     * @return false
     */
    public function decline($dc) {
        $recordID = $dc->id;
        if(!empty($recordID)){
            $oRecord = SchikoSurveyModel::findByPk($recordID);
            $oRecord->setRegisterStatus('cancel',1);
        }
        Controller::redirect('contao?do=survey');
    }

    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getCateringOptions(DataContainer $dc): array {
        $catID = $dc->activeRecord->category;

        $aOptions = [];
        if(!empty($catID)){
            $oCategory = SfCategoriesModel::findByPk($catID);
            if(!empty($oCategory->schiko_rate)) {
                $aRates = unserialize($oCategory->schiko_rate);
                foreach ($aRates as $key => $val) {

                    $entry = $val['sr_einsatz'] == 'half' ? '1/2 Tag' : '1 Tag';
                    $food = $val['sr_food'] == 'snack' ? 'Zwischenverpflegung' : 'Hauptmahlzeit';
                    $aOptions[$key] = sprintf('%s | %s',$entry,$food);
                }
            }
        }
        return $aOptions;
    }

    /**
     * @param DataContainer $data
     * @return DataContainer
     */
    public function calcHonors(DataContainer $data) {
        $record     = $data->activeRecord;
        $iCategory  = $record->category;
        $total      = 0;
        $fee        = 0;
        $directionFee = 0;
        $travel_distance   = 0;
        $blnRecalcTravelDistance = empty($record->calcTravelDistance)? NULL : 1;

        if(!empty($iCategory)) {
            $oCategory = SfCategoriesModel::findByPk($iCategory);
            if ($oCategory->schiko_rate) {
                $aRates = StringUtil::deserialize($oCategory->schiko_rate);
                if(isset($aRates[0]['sr_value']) AND $aRates[0]['sr_value'] != '')
                {
                    $fee = $aRates[0]['sr_value'];
                }
            }
        }

        SchikoSurveyModel::calcHonorByRound($record->round,$record->id,$blnRecalcTravelDistance);
        return $data;
    }

    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getRoundOptions(DataContainer $dc): array{
        //Get Active Seasons
        $oSeasons = SfSeasonModel::findBy('active',1);
        $aSeasons = [];
        $aReturn  = [];

        if(null !== $oSeasons) {
            foreach ($oSeasons as $key => $val) {
                $aSeasons[] = $val->season;
            }
        }

        //get Rounds
        if(!empty($aSeasons)) {
            $oRounds = SfRoundsModel::findBy(['season IN (?)'], [implode(",",$aSeasons)], ['order' => 'season desc, date asc']);
        }

        //parse Rounds
        if(null !== $oRounds) {
            foreach($oRounds as $key => $round) {
                $aReturn[$round->id] = sprintf('%s | %s %s Uhr - %s', $round->season, Date::parse('d.m.y', $round->date), Date::parse('H:i', $round->time), $round->location);
            }
        }
        return $aReturn;
    }

    /**
     * @param string $value
     * @param DataContainer $dc
     * @return mixed|string
     */
    public function addPorto(string $value, DataContainer $dc){

        //Set Porto if referee == manager
        if($value === 'manager' && empty($dc->activeRecord->porto)) {
            $oSurvey = SchikoSurveyModel::findByPk($dc->activeRecord->id);
            $oSurvey->porto = 2;
            $oSurvey->save();
        }
        return $value;
    }

    public static function getActiveSeason()
    {
        $oSeason = SfSeasonModel::findOneBy('active',1,['order'=>'tstamp desc']);
        if(!empty($oSeason))
        {
            return $oSeason->id;
        }
        return null;
    }

}

//Set Default Filter for List, show only active Season
$activeSeason  = tl_schiko_survey::getActiveSeason();
$strFilterList = [];
if(null !== $activeSeason)
{
    $GLOBALS['TL_DCA']['tl_schiko_survey']['list']['sorting']['filter']    = [
        ['season=?',$activeSeason]
    ];
}
