<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use \Memo\SchikoBundle\Model\SchikoSurveyModel;

PaletteManipulator::create()
    ->addLegend('schiko_legend','season_legend',PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_driving_expenses', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_send_decline_mail', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_decline_notification', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_sf_season');



/**
 * Table tl_sf_season
 */

//Add save Callback
$GLOBALS['TL_DCA']['tl_sf_season']['config']['onsubmit_callback'][] = ['tl_sf_season_schiko', 'calcHonors'];

//Add export Button
$GLOBALS['TL_DCA']['tl_sf_season']['list']['operations']['xlsSeasonTotals'] = [
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_season']['xlsSeasonTotals'],
    'href'					=> 'key=xlsSeasonTotals',
    'icon'					=> '/bundles/schiko/icons/totals.svg',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_season_schiko', 'exportXlsSeasonTotalLink')
];

$GLOBALS['TL_DCA']['tl_sf_season']['list']['operations']['xlsTotals'] = [
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_season']['pdfInsetPlan'],
    'href'					=> 'key=xlsTotals',
    'icon'					=> '/bundles/schiko/img/pdf.svg',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_season_schiko', 'exportXlsTotal')
];

$GLOBALS['TL_DCA']['tl_sf_season']['list']['operations']['xlsInsets'] = [
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_season']['xlsInsets'],
    'href'					=> 'key=xlsInsets',
    'icon'					=> '/bundles/schiko/img/referee.png',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_season_schiko', 'exportXlsInsets')
];

$GLOBALS['TL_DCA']['tl_sf_season']['fields']['schiko_driving_expenses'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'rgxp'=>'digit'),
    'sql'                     => "varchar(32) NOT NULL default 0.5"
];


$GLOBALS['TL_DCA']['tl_sf_season']['fields']['schiko_send_decline_mail'] = [
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'clr w50 m12','includeBlankOption' => false,'choosen'=>false,'mandatory'=>false),
    'sql'                       => "int(1) NOT NULL default 0"
];

$GLOBALS['TL_DCA']['tl_sf_season']['fields']['schiko_decline_notification'] = [
    'inputType'               => 'select',
    'foreignKey'              => 'tl_nc_notification.title',
    'relation'                => ['lazy'=>'lazy','type'=>'hasOne'],
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => true,'choosen'=>true,'mandatory'=>false),
    'sql'                     => "varchar(255) NOT NULL default ''",
];


use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use \Memo\ModSwissfaustballBundle\Model\SfRoundsModel;

class tl_sf_season_schiko extends Backend
{

    /**
     * @param $data
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @param $table
     * @return string
     */
    public function exportXlsTotal($data, $href, $label, $title, $icon, $attributes, $table)
    {
        return sprintf('<a href="%s" title="%s" class=""><img src="%s" width="16" height="16" alt="%s"></a>', $this->addToUrl($href . '&amp;id=' . $data['id']), $label, $icon, $title);
    }

    /**
     * @todo XLS Export function
     * @param DataContainer $dc
     * @return string
     */
    public function exportRoundTotals(DataContainer $dc) {

        \Controller::redirect('/schiko/export/entry/pdf/'.$dc->id);
        return false;
    }

    /**
     * @param $data
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @param $table
     * @return string
     */
    public function exportXlsSeasonTotalLink($data,$href,$label,$title,$icon,$attributes,$table) {
        return sprintf('<a href="/schiko/export/season/billing/xls/%s" title="%s" class=""><img src="%s" width="16" height="16" alt="%s"></a>',$data['id'],$label,$icon,$title);
    }

    /**
     * @param $
     * @return void
     */
    public function exportXlsInsets($data,$href,$label,$title,$icon,$attributes,$table) {
        return sprintf('<a href="/contao/schiko/export/insets/%s" title="%s" class=""><img src="%s" width="12" height="16" alt="%s"></a>',$data['id'],$label,$icon,$title);
    }

    public function calcHonors(DataContainer $dc)
    {
        $record      = $dc->activeRecord;
        $oRoundModel = new SfRoundsModel();
        $aRounds = $oRoundModel->getRoundIdsBySeason($record->id);

        //Recalc all Honors by round
        foreach ($aRounds as $key => $roundID) {
            SchikoSurveyModel::calcHonorByRound($roundID,null);
        }

    }
}
