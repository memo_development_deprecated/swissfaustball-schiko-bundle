<?php


$GLOBALS['TL_DCA']['tl_module']['palettes']['memo_schiko_survey'] =
    '{title_legend},name,type,season,bln_register,bln_view_by_cat,bln_hide_played_rounds;{template_legend},customTpl;'
;

$GLOBALS['TL_DCA']['tl_module']['fields']['season'] = [
    'exclude'       => true,
    'filter'		=> false,
    'sorting'       => false,
    'inputType'     => 'select',
    'foreignKey'    => 'tl_sf_season.season',
    'options_callback' => ['tl_schiko_survey_module','getSeasonOptions'],
    'eval'          => array( 'mandatory'=>true, 'chosen'=>true,'tl_class'=>'w50 clr'),
    'sql'           => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_module']['fields']['bln_register'] = array
(
    'sorting'				  => false,
    'exclude'                 => false,
    'filter'                  => true,
    'inputType'               => 'checkbox',
    'eval'                    => array('doNotCopy'=>true,'tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['bln_view_by_cat'] = array
(
    'sorting'				  => false,
    'exclude'                 => false,
    'filter'                  => false,
    'inputType'               => 'checkbox',
    'eval'                    => array('doNotCopy'=>true,'tl_class'=>'w50 m12 clr'),
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['bln_hide_played_rounds'] = array
(
    'sorting'				  => false,
    'exclude'                 => false,
    'filter'                  => false,
    'inputType'               => 'checkbox',
    'eval'                    => array('doNotCopy'=>true,'tl_class'=>'w50 m12'),
    'sql'                     => "char(1) NOT NULL default ''"
);

use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;

class tl_schiko_survey_module extends Backend {


    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Contao\BackendUser', 'User');
    }


    /**
     * @param DataContainer $dc
     * @return array
     */
    public function getSeasonOptions(DataContainer $dc): array {
        $aReturn = [];

        $oSeason = SfSeasonModel::findAll(['order'=>'season desc']);
        if($oSeason) {
            foreach($oSeason as $key => $val) {
                if($val->active == 1) {
                    $aReturn['active'][$val->id] = $val->season;
                }else{
                    $aReturn['archive'][$val->id] = $val->season;
                }
            }
        }
        return $aReturn;
    }

}
