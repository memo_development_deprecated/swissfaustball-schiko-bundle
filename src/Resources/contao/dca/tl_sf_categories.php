<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;


PaletteManipulator::create()
    ->addLegend('schiko_legend','category_relegation_legend',PaletteManipulator::POSITION_BEFORE)
    ->addField('schiko_commission', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_referee_count', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_rate', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_sf_categories');


\Contao\System::loadLanguageFile('tl_sf_categories', 'de');

/**
 * Table tl_sf_categories
 */
$GLOBALS['TL_DCA']['tl_sf_categories']['fields']['schiko_rate'] = [
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'tl_class' => 'clr w50',
        'maxCount' => 1,
        'columnFields' => [
            'sr_einsatz'      => [
                'label'     => $GLOBALS['TL_LANG']['tl_sf_categories']['sr_einsatz'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'includeBlankOption' => true,
                ],
                'options'   => [
                    'half' => '1/2 Tag',
                    'full' => '1 Tag',
                ],
            ],
            'sr_value' => [
                'label'     => $GLOBALS['TL_LANG']['tl_sf_categories']['sr_value'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'includeBlankOption' => true,
                ],
                'options'   => [
                    '20',
                    '30',
                    '40',
                    '50',
                    '60',
                    '100',
                ],
            ],
            'sr_food' => [
                'label'     => $GLOBALS['TL_LANG']['tl_sf_categories']['sr_food'],
                'exclude'   => true,
                'inputType' => 'select',
                'eval'      => [
                    'includeBlankOption' => true,
                ],
                'options'   => [
                    'snack' => 'Zwischenverpflegung',
                    'meal' => 'Volle Verpflegung',
                ],
            ],
        ],
    ],
    'sql'       => 'blob NULL',
];

$GLOBALS['TL_DCA']['tl_sf_categories']['fields']['schiko_referee_count'] = [
    'inputType'               => 'select',
    'options'                 => [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
    'eval'                    => array('tl_class'=>'w50','rgxp'=>'digit','includeBlankOption' => true),
    'sql'                     => "int(10) unsigned NOT NULL default 0",
];

$GLOBALS['TL_DCA']['tl_sf_categories']['fields']['schiko_commission'] = [
    'exclude'                 => false,
    'filter'                  => true,
    'inputType'               => 'select',
    'foreignKey'              => 'tl_schiko_commission.name',
    'relation'                => array('type'=>'hasOne', 'load'=>'lazy'),
    'eval'                    => array('mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'chosen'=>true),
    'reference'               => &$GLOBALS['TL_LANG']['COLS'],
    'sql'                     => "varchar(32) NOT NULL default ''"
];
