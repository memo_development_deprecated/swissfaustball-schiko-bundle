<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;

PaletteManipulator::create()
    ->addLegend('schiko_legend','chmod_legend',PaletteManipulator::POSITION_AFTER)
    ->addField('schiko_google_maps_api_key', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_openroute_key', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_settings');


$GLOBALS['TL_DCA']['tl_settings']['fields']['schiko_google_maps_api_key'] = array(
        'inputType' => 'text',
        'eval'      => array('mandatory'=>false, 'tl_class'=>'clr w50'),
        'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['schiko_openroute_key'] = array(
        'inputType' => 'text',
        'eval'      => array('mandatory'=>false, 'tl_class'=>'clr w50'),
        'sql' => "varchar(255) NOT NULL default ''"
);

