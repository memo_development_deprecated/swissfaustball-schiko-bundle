<?php

use Contao\CoreBundle\DataContainer\PaletteManipulator;


PaletteManipulator::create()
    ->addLegend('schiko_legend','rounds_legend',PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_organizer', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('org_address', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('org_zipcode', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('org_place', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('org_lat', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('org_lng', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('reg_closed', 'schiko_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_sf_rounds');



/**
 * extend Table tl_sf_rounds
 */

//load Geocoordinates from address
$GLOBALS['TL_DCA']['tl_sf_rounds']['config']['onsubmit_callback'][] = ['tl_sf_rounds_schiko','saveGeoCoordinates'];

//Add export Button
$GLOBALS['TL_DCA']['tl_sf_rounds']['list']['operations']['xlsTotals'] = [
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_rounds']['download'],
    'href'					=> 'key=xlsTotals',
    'icon'					=> '/bundles/schiko/icons/totals.svg',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_rounds_schiko', 'exportXlsTotal')
];

//Add Mail Icon
$GLOBALS['TL_DCA']['tl_sf_rounds']['list']['operations']['activate_mailing'] = [
    'label'					=> &$GLOBALS['TL_LANG']['tl_sf_rounds']['activate_mailing'],
    'href'					=> 'key=xlsTotals',
    'icon'					=> '/bundles/schiko/icons/envelop.svg',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'		=> array('tl_sf_rounds_schiko', 'mailingStatus')
];


$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['schiko_organizer'] = [
    'inputType'               => 'select',
    'foreignKey'              => 'tl_mod_club.name',
    'relation'                => ['lazy'=>'lazy','type'=>'hasOne'],
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => true,'choosen'=>true,'mandatory'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''",
];



$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['org_address'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'mandatory'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['org_zipcode'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'mandatory'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''",
];
$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['org_place'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'mandatory'=>true),
    'sql'                     => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['org_lat'] = [
    'xlabel'                   => [['tl_sf_rounds_schiko','getCoordLabel']],
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'disabled'=>false),
    'sql'                     => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['org_lng'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'disabled'=>false),
    'sql'                     => "varchar(255) NOT NULL default ''",
];

$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['sent'] = [
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50','includeBlankOption' => false,'choosen'=>false,'mandatory'=>false),
    'sql'                       => "int(1) NOT NULL default 0"
];

$GLOBALS['TL_DCA']['tl_sf_rounds']['fields']['reg_closed'] = [
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50 mt12 clr','includeBlankOption' => false,'choosen'=>false,'mandatory'=>false),
    'sql'                       => "int(1) NOT NULL default 0"
];


use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\SchikoBundle\Service\GeocoderService;

class tl_sf_rounds_schiko extends Backend
{

    /**
     * @param DataContainer $dc
     * @return string
     */
    public function getCoordLabel(DataContainer $dc) {
        $strDescription = sprintf(' | <a href="https://maps.google.com/?q=%s,%s" target="_blank">« Koordinaten prüfen »</a>',$dc->activeRecord->org_lat,$dc->activeRecord->org_lng);
        return $strDescription;
    }



    /**
     * @param DataContainer $dc
     * @toDo get LIve Coordinates
     * @return bool
     */
    public function saveGeoCoordinates(DataContainer $dc): bool {
        $strAddress = sprintf('%s, %s %s',$dc->activeRecord->org_address,$dc->activeRecord->org_zipcode,$dc->activeRecord->org_place);
        $oRound = SfRoundsModel::findByPk($dc->activeRecord->id);

        //Geocode Address if no coordinates are available
        if(empty($oRound->org_lng) OR empty($oRound->org_lat)) {
            $oService = new GeocoderService();

            if (!empty($strAddress)) {
                $result = $oService->geocodeAddress($strAddress);
                if(null !== $result) {
                    $first = $result->first();
                    if (!empty($first) && method_exists($first,'getCoordinates')) {
                        $oRound->org_lng = $first->getCoordinates()->getLongitude();
                        $oRound->org_lat = $first->getCoordinates()->getLatitude();
                        $oRound->save();
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $data
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @param $table
     * @return string
     */
    public function exportXlsTotal($data,$href,$label,$title,$icon,$attributes,$table) {
        return sprintf('<a href="/schiko/export/offer/pdf/%s" title="%s" class=""><img src="%s" width="16" height="16" alt="%s"></a>',$data['id'],$label,$icon,$title);
    }

    /**
     * @param $data
     * @param $href
     * @param $label
     * @param $title
     * @param $icon
     * @param $attributes
     * @param $table
     * @return string
     */
    public function mailingStatus($data,$href,$label,$title,$icon,$attributes,$table) {

        if($data['sent'] == 1) {
            return sprintf('<a href="/contao/schiko/notify/round/deactivate/%s" title="%s" class="schiko-mail-icon"><img src="%s" width="16" height="16" alt="%s"></a>', $data['id'], 'Versand freigegeben | deaktivieren?', '/bundles/schiko/icons/envelop-active.svg', 'Versand freigegeben');
        }elseif($data['sent'] == 2) {
            return sprintf('<a title="%s" class="schiko-mail-icon"><img src="%s" width="16" height="16" alt="%s"></a>', 'Versand abgeschlossen', '/bundles/schiko/icons/envelop-sent.svg', 'Versand abgeschlossen');
        }elseif($data['sent'] == 5) {
            return sprintf('<a title="%s" class="schiko-mail-icon"><img src="%s" width="16" height="16" alt="%s"></a>', 'Versand läuft', '/bundles/schiko/icons/envelop-progress.svg', 'Versand läuft');
        }elseif($data['sent'] == 9) {
            return sprintf('<a title="%s" class="schiko-mail-icon"><img src="%s" width="16" height="16" alt="%s"></a>', 'Fehler beim Versand', '/bundles/schiko/icons/envelop-error.svg', 'Versand fehlerhaft');
        }else{
            return sprintf('<a href="/contao/schiko/notify/round/activate/%s" title="%s" class="schiko-mail-icon"><img src="%s" width="16" height="16" alt="%s"></a>', $data['id'], 'Versand noch nicht freigegeben | aktivieren?', $icon, 'Versand noch nicht freigegeben');
        }
    }

    /**
     * @todo XLS Export function
     * @param DataContainer $dc
     * @return string
     */
    public function exportRoundTotals(DataContainer $dc) {
        $oRound = SfRoundsModel::findByPk($dc->id);

        return "<pre>".var_export($oRound,1)."</pre>";
    }

}
