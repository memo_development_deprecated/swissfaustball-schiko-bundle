<?PHP

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */




/**
 * Table tl_schiko_commission
 */

$GLOBALS['TL_DCA']['tl_schiko_commission'] = [
    'config' => [
        'dataContainer' => 'Table',
        'enableVersioning' => true,
        'notCopyable'  => false,
        'sql' => [
            'keys' => [
                'id' => 'primary',
            ],
        ],
    ],
    'list' => [
        'sorting' => [
            'mode'      => 2,
            'fields'    =>  ['name'],
            'headerFields'=>['id','name','shortname','member'],
            'panelLayout' => 'filter;sort,search,limit',

        ],
        'label' => [
            'fields'=>[
                'id',
                'name',
                'shortname',
                'member'
            ],
            'label_callback' => ['tl_schiko_commission','getColumnLabels'],
            'showColumns' => true,
            'format' => '%s'
        ],
        'global_operations' => array
        (
            'all' => array
            (
                'href'                => 'act=select',
                'class'               => 'header_edit_all',
                'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'href'                => 'act=edit',
                'icon'                => 'edit.svg'

            ),
			'copy' => array
			(
				'href'					=> 'act=copy',
				'icon'					=> 'copy.gif'
			),
			'delete' => array
			(
				'href'					=> 'act=delete',
				'icon'					=> 'delete.gif',
				'attributes'			=> 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
			),
            'toggle' => array
            (
                'icon'                => 'visible.svg',
                'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback'     => array('tl_schiko_commission', 'toggleIcon'),
                'showInHeader'        => true
            ),
            'show' => array
            (
                'href'                => 'act=show',
                'icon'                => 'show.svg'
            )
        ),
    ],
    // Palettes
    'palettes' => array
    (
        '__selector__'                => array(''),
        'default'                     =>   '{commission_legend},name,shortname,description;
                                            {commission_contact_legend},member;
                                            {expert_legend:hide},cssID;{invisible_legend:hide},published;'
    ),
    'fields' => [
        'id' => array(
            'label'                   => array('ID'),
            'sql'                     => "int(10) unsigned NOT NULL auto_increment"
        ),
        'tstamp' => array(
            'sorting'   => true,
            'flag'      => 12,
            'inputType' => 'text',
            'sql'       => "int(10) unsigned NOT NULL default '0'"
        ),
        'name' => array(
            'inputType' => 'text',
            'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'shortname' => array(
            'inputType' => 'text',
            'eval'      => array('mandatory'=>false, 'tl_class'=>'w50'),
            'sql' => "varchar(255) NOT NULL default ''"
        ),
        'description' => array(
            'exclude'                 => true,
            'search'                  => true,
            'inputType'               => 'textarea',
            'eval'                    => array('mandatory'=>false, 'rte'=>'tinyMCE', 'helpwizard'=>true,'tl_class'=>'clr'),
            'explanation'             => 'insertTags',
            'sql'                     => "mediumtext NULL default ''"
        ),
        'member' => array
        (
            'exclude'                 => true,
            'search'                  => true,
            'sorting'                 => true,
            'inputType'     => 'select',
            'foreignKey'    => 'tl_member.CONCAT(firstname," ",lastname)',
            'relation'      => ['load'=>'eager','type'=>'hasMany', 'table'=>'tl_member','field'=>'id'],
            'eval'          => array('chosen'=>true, 'mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'disabled'=>false, 'multiple' => true),
            'sql'           => "varchar(255) NOT NULL default ''"
        ),

        'cssID' => array
        (
            'exclude'                 => true,
            'inputType'               => 'text',
            'eval'                    => array('multiple'=>true, 'size'=>2, 'tl_class'=>'w50 clr'),
            'sql'                     => "varchar(255) NOT NULL default ''"
        ),
         'published' => array
        (
	        'sorting'				  => true,
            'exclude'                 => true,
            'filter'                  => true,
            'inputType'               => 'checkbox',
            'eval'                    => array('doNotCopy'=>true),
            'sql'                     => "char(1) NOT NULL default ''"
        )
    ]

];

class tl_schiko_commission extends Backend {

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Contao\BackendUser', 'User');
    }


    /**
     * Return the "toggle visibility" button
     *
     * @param array  $row
     * @param string $href
     * @param string $label
     * @param string $title
     * @param string $icon
     * @param string $attributes
     *
     * @return string
     */
    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        if (Input::get('tid'))
        {
            $this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1), (func_num_args() <= 12 ? null : func_get_arg(12)));
            $this->redirect($this->getReferer());
        }

        // Check permissions AFTER checking the tid, so hacking attempts are logged
        if (!$this->User->hasAccess('tl_schiko_commission::published', 'alexf'))
        {
            return '';
        }

        $href .= '&amp;tid=' . $row['id'] . '&amp;state=' . ($row['published'] ? '' : 1);

        if (!$row['published'])
        {
            $icon = 'invisible.svg';
        }

        return '<a href="' . $this->addToUrl($href) . '" title="' . StringUtil::specialchars($title) . '"' . $attributes . '>' . Image::getHtml($icon, $label, 'data-state="' . ($row['published'] ? 1 : 0) . '"') . '</a> ';
    }


    /**
     * Disable/enable a user group
     *
     * @param integer       $intId
     * @param boolean       $blnVisible
     * @param DataContainer $dc
     */
    public function toggleVisibility($intId, $blnVisible, DataContainer $dc=null)
    {
        // Set the ID and action
        Input::setGet('id', $intId);
        Input::setGet('act', 'toggle');

        if ($dc)
        {
            $dc->id = $intId; // see #8043
        }

        // Check the field access
        if (!$this->User->hasAccess('tl_schiko_commission::published', 'alexf'))
        {
            throw new AccessDeniedException('Not enough permissions to publish/unpublish news item ID ' . $intId . '.');
        }

        $objRow = $this->Database->prepare("SELECT * FROM tl_schiko_commission WHERE id=?")
            ->limit(1)
            ->execute($intId);

        if ($objRow->numRows < 1)
        {
            throw new AccessDeniedException('Invalid item ID ' . $intId . '.');
        }

        // Set the current record
        if ($dc)
        {
            $dc->activeRecord = $objRow;
        }

        $objVersions = new Versions('tl_competition', $intId);
        $objVersions->initialize();

        // Trigger the save_callback
        if (is_array($GLOBALS['TL_DCA']['tl_schiko_commission']['fields']['published']['save_callback'] ?? null))
        {
            foreach ($GLOBALS['TL_DCA']['tl_schiko_commission']['fields']['published']['save_callback'] as $callback)
            {
                if (is_array($callback))
                {
                    $this->import($callback[0]);
                    $blnVisible = $this->{$callback[0]}->{$callback[1]}($blnVisible, $dc);
                }
                elseif (is_callable($callback))
                {
                    $blnVisible = $callback($blnVisible, $dc);
                }
            }
        }

        $time = time();

        // Update the database
        $this->Database->prepare("UPDATE tl_schiko_commission SET tstamp=$time, published='" . ($blnVisible ? '1' : '') . "' WHERE id=?")
            ->execute($intId);

        if ($dc)
        {
            $dc->activeRecord->tstamp = $time;
            $dc->activeRecord->published = ($blnVisible ? '1' : '');
        }

        $objVersions->create();

        // The onsubmit_callback has triggered scheduleUpdate(), so run generateFeed() now
        $this->generateFeed();

        if ($dc)
        {
            $dc->invalidateCacheTags();
        }
    }

    public function getColumnLabels(array $record,string $label, DataContainer $dc, array $aLabels){

        $aMembers     = \StringUtil::deserialize($record['member']);
        $oMembers     = MemberModel::findMultipleByIds($aMembers);
        $aLabels[3]   = '';
        if($oMembers) {
            foreach ($oMembers as $key => $val) {
                if($key > 0) { $aLabels[3] .= ', ';}
                $aLabels[3] .= sprintf('%s %s', $val->firstname, $val->lastname);
            }
        }
        return $aLabels;
    }
}

?>
