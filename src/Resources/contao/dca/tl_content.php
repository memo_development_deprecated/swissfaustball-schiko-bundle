<?php

$GLOBALS['TL_DCA']['tl_content']['palettes']['MemoSchikoGalleryElement'] = '
{type_legend},type;
{gallery_legend},headline,MemoSchikoGalleryMembers,MemoSchikoGalleryColumns,size,MemoSchikoFileUpload;
{template_legend:hide},customTpl;
{protected_legend:hide},protected;
{expert_legend:hide},guests,cssID;
{invisible_legend:hide},invisible,start,stop;
';


$GLOBALS['TL_DCA']['tl_content']['fields']['MemoSchikoGalleryMembers'] = array(
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'checkboxWizard',
    'options_callback'        => ['schiko_content_class','getSchikoMembers'],
    'eval'                    => ['tl_class'=>'clr colums4','isAssociative'=>true,'multiple'=>true,'chosen'=>true],
    'sql'                     => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_content']['fields']['MemoSchikoGalleryColumns'] = array(
    'exclude'                 => true,
    'search'                  => true,
    'inputType'               => 'select',
    'options'                 => [1,2,3,4,5,6,7,8,9,10,11,12],
    'eval'                    => ['tl_class'=>'clr w50','rgxp'=>'digit'],
    'sql'                     => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_content']['fields']['MemoSchikoFileUpload'] = array(
        'exclude'                 => true,
        'inputType'               => 'fileTree',
        'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio','tl_class'=>'clr','extensions' => Config::get('allowedDownload')),
        'sql'                     => "binary(16) NULL"
);


class schiko_content_class extends Backend {

    function getSchikoMembers(DataContainer $dc) {

        $arrData = [];
        $arrTmp = [];
        $oMember = MemberModel::findAll(['order'=>'lastname']);
        foreach($oMember as $key => $val)
        {
            $aGroups = unserialize($val->groups);
            $aTags = unserialize($val->schiko_tags);
            $categorie = empty($aTags[0])? '' : strtoupper($aTags[0]);

            //Add only Schiedsrichter
            if(in_array(2,$aGroups)) {
                $arrTmp[$categorie][$val->id] = $val->firstname . ' ' . $val->lastname ;
            }
        }
        ksort($arrTmp);
        foreach($arrTmp as $cat => $data) {
            foreach($data as $key => $val) {
                $arrData[$key] = empty($cat) ? $val : $cat . ' | ' . $val;
            }
        }

        return $arrData;
    }
}
