<?php

/**
* extend Table tl_member
*/

use Contao\CoreBundle\DataContainer\PaletteManipulator;


PaletteManipulator::create()
    ->addLegend('referee_legend','contact_legend',PaletteManipulator::POSITION_AFTER)
    ->addField('rel_tl_mod_club', 'referee_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('club', 'referee_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('count_for_club', 'referee_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('schiko_tags', 'referee_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('brevet_year', 'referee_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_member');

PaletteManipulator::create()
    ->addLegend('stats_legend','referee_legend',PaletteManipulator::POSITION_AFTER)
    ->addField('entry_field', 'stats_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('entry_hall', 'stats_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('entry_total', 'stats_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->addField('entry_last', 'stats_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_member');

PaletteManipulator::create()
    ->addField('singleSRC', 'personal_legend', \Contao\CoreBundle\DataContainer\PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_member');




//Replace Fax with Business Phone
$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] = str_replace("fax,","phone_business,",$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);

//Replace State & Country
$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] = str_replace("state,country","country,coordinates",$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);

//Remove Website & Language
$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] = str_replace("website,","",$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);
$GLOBALS['TL_DCA']['tl_member']['palettes']['default'] = str_replace(",language","",$GLOBALS['TL_DCA']['tl_member']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_member']['fields']['phone_business'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp' => 'phone','feEditable'=>true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['coordinates'] = array(
    'xlabel'    => [['tl_member_schiko','getCoordLabel']],
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','alwaysSave'=>false),
    'sql' => "varchar(255) NOT NULL default ''",
    'save_callback' => [['tl_member_schiko','getCoordinates']]
);


//Set mandatory
$GLOBALS['TL_DCA']['tl_member']['fields']['dateOfBirth']['eval']['mandatory'] = true;
$GLOBALS['TL_DCA']['tl_member']['fields']['street']['eval']['mandatory'] = true;
$GLOBALS['TL_DCA']['tl_member']['fields']['postal']['eval']['mandatory'] = true;
$GLOBALS['TL_DCA']['tl_member']['fields']['city']['eval']['mandatory'] = true;
$GLOBALS['TL_DCA']['tl_member']['fields']['mobile']['eval']['mandatory'] = true;

$GLOBALS['TL_DCA']['tl_member']['fields']['count_for_club'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>true, 'tl_class'=>'w50','feEditable'=>true,'disabled'=>false),
    'sql' => "varchar(255) NOT NULL default ''"
);

//Check if active Season is started. If true disable count_for_club field on fe member form.
$blnStarted = \Memo\ModSwissfaustballBundle\Model\SfSeasonModel::isActiveSeasonStarted();
$GLOBALS['TL_DCA']['tl_member']['fields']['count_for_club']['eval']['disabled'] = $blnStarted;
$GLOBALS['TL_DCA']['tl_member']['fields']['count_for_club']['eval']['mandatory'] = !$blnStarted;

$GLOBALS['TL_DCA']['tl_member']['fields']['club'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>true, 'tl_class'=>'w50','feEditable'=>true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['rel_tl_mod_club'] = array(
    'inputType' => 'select',
    'foreignKey' => 'tl_mod_club.name',
    'relation'  => array('type'=>'hasOne', 'load'=>'lazy'),
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','includeBlankOption'=>true,'feEditable'=>true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['brevet_year'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','feEditable'=>true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['entry_field'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['entry_hall'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['entry_total'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'digit'),
    'sql' => "varchar(255) NOT NULL default ''",
    'save_callback' => ['tl_member_schiko','save_total']
);

$GLOBALS['TL_DCA']['tl_member']['fields']['entry_last'] = array(
    'inputType' => 'text',
    'eval'      => array('mandatory'=>false, 'tl_class'=>'w50','rgxp'=>'date'),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['schiko_tags'] = array(
    'filter'    => true,
    'inputType' => 'select',
    'options'   => ['i' => 'I-Schiri','a' => 'A-Schiri','b' => 'B-Schiri','z'=>'Z-Schiri','n'=>'N-Schiri Reserve','zv'=>'Funktionär','r' => 'ehemalige Schiedsrichter'],
    'eval'      => array('mandatory'=>false, 'chosen'=>true, 'multiple'=>true, 'tl_class'=>'w50','includeBlankOption'=>true),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_member']['fields']['singleSRC'] = array
(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>false, 'tl_class'=>'clr'),
    'sql'                     => "binary(16) NULL"
);


/**
 * Add export Card Button
 */

$GLOBALS['TL_DCA']['tl_member']['list']['operations']['generate_id_card'] = [
    'href'                => 'key=genIdCard',
    'icon'                => 'public/bundles/schiko/icons/idcard.svg',
    'class'               => 'export_id_card',
    'attributes'			=> 'onclick="Backend.getScrollOffset();"',
    'button_callback'     => array('tl_member_schiko', 'genIdCard')
];

use Memo\SchikoBundle\Service\GeocoderService;

class tl_member_schiko extends Backend {


    /**
     * @param DataContainer $dc
     * @return string
     */
    public function getCoordLabel(DataContainer $dc) {
        $strDescription = sprintf(' | <a href="https://maps.google.com/?q=%s" target="_blank">« Koordinaten prüfen »</a>',$dc->activeRecord->coordinates);
        return $strDescription;
    }

    /**
     * Import the back end user object
     */
    public function __construct()
    {
        parent::__construct();
        $this->import('Contao\BackendUser', 'User');
    }

    /**
     * @param $data
     * @param DataContainer $dc
     */
    public function getCoordinates($data, DataContainer $dc){
        //Geocode Address if no coordinates are available
        if(empty($data)) {
            $oService = new GeocoderService();
            $country  = empty($dc->activeRecord->country) ? 'ch' : $dc->activeRecord->country;
            $address  = sprintf('%s %s %s, %s', $dc->activeRecord->street, $dc->activeRecord->postal, $dc->activeRecord->city, $country);
            $data = '';

            if (!empty($address)) {
                $result = $oService->geocodeAddress($address);
                if(null !== $result) {
                    $result = $result->first();
                    if (!empty($result->getCoordinates())) {
                        $data = sprintf('%s,%s', $result->getCoordinates()->getLatitude(), $result->getCoordinates()->getLongitude());
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @param $data
     * @param DataContainer $dc
     */
    public function save_total($data, DataContainer $dc){

    }

    public function genIdCard($row, $href, $label, $title, $icon){

        $aGroups = [];
        if(!empty($row['groups'])) {
            $aGroups = unserialize($row['groups']);
        }

        if(!in_array(2,$aGroups)) {
            return '–';
        }

        $url = System::getContainer()->get('router')->generate('Memo\SchikoBundle\Controller\BackendControllerExportRefereeId', array('user'=>$row['id']));
        return '<a href="' . StringUtil::specialcharsUrl($url) . '" title="' . StringUtil::specialchars('ID-Card generieren') . '" target="_blank" class="schiko_export_idcard">' . Image::getHtml($icon, $label) . '</a> ';
    }
}
