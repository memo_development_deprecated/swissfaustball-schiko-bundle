<?php declare(strict_types=1);

/**
 * @package   Swissfaustball SCHIKO Bundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add back end modules
 */
array_insert($GLOBALS['BE_MOD']['memo_schiko'], 100, array
(
	'schiko' => array
	(
		'tables'       => array(
            'tl_schiko_commission',
            'tl_schiko_referee'
		),
	),
    'survey' => array
    (
        'tables'       => array(
            'tl_schiko_survey',
            'tl_member',
        ),
    )
));

$GLOBALS['BE_MOD']['swissfaustball']['tl_sf_rounds']['xlsTotals'] = ['tl_sf_rounds_schiko', 'exportXlsTotal'];
$GLOBALS['BE_MOD']['swissfaustball']['tl_sf_season']['xlsSeasonTotals'] = ['tl_sf_season_schiko', 'exportXlsSeasonTotals'];
$GLOBALS['BE_MOD']['swissfaustball']['tl_sf_season']['xlsTotals'] = ['tl_sf_season_schiko', 'exportRoundTotals'];
$GLOBALS['BE_MOD']['swissfaustball']['tl_sf_season']['xlsInsets'] = ['tl_sf_season_schiko', 'exportXlsInsets'];
$GLOBALS['BE_MOD']['memo_schiko']['survey']['accept'] = ['tl_schiko_survey', 'accept'];
$GLOBALS['BE_MOD']['memo_schiko']['survey']['decline'] = ['tl_schiko_survey', 'decline'];

/**
 * Add front end modules and Custom Elements
 */
$GLOBALS['FE_MOD']['memo_schiko'] = array
(
    'memo_schiko_survey'    => 'Memo\SchikoBundle\Module\SchikoSurveyModule'
);

$GLOBALS['TL_CTE']['MEMO Customs']['MemoSchikoGalleryElement']  = 'Memo\SchikoBundle\Elements\MemoSchikoGalleryElement';


/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_schiko_commission']       = 'Memo\SchikoBundle\Model\SchikoCommissionModel';
$GLOBALS['TL_MODELS']['tl_schiko_survey']       = 'Memo\SchikoBundle\Model\SchikoSurveyModel';
$GLOBALS['TL_MODELS']['tl_member']       = 'Memo\SchikoBundle\Model\SchikoMemberModel';



/**
 * Backend CSS
 */
if(TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS'][]        = '/bundles/schiko/css/backend.css';
}
