<?php

    $GLOBALS['TL_LANG']['MOD']['memo_schiko'] 			= ["SCHIKO – DB"];
    $GLOBALS['TL_LANG']['MOD']['schiko'] 				= ["Kommissionen"];
    $GLOBALS['TL_LANG']['MOD']['referee'] 				= ["Schiedsrichter"];
    $GLOBALS['TL_LANG']['MOD']['survey'] 				= ["Umfragen"];
    $GLOBALS['TL_LANG']['MOD']['export_referee'] 				= ["XLS Export Schiedsrichter"];
