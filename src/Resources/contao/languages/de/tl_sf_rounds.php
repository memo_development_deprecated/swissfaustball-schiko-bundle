<?php


$GLOBALS['TL_LANG']['tl_sf_rounds']['schiko_legend']    = 'SCHIKO - Datenbank';
$GLOBALS['TL_LANG']['tl_sf_rounds']['schiko_organizer'] = ['Organisator (Verein)','Organisator, wird für Abrechnung verwendet.'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['org_address']      = ['Adresse Spielort','wird für Navigation verwendet (KM Geld Schiedsrichter)'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['org_zipcode']      = ['PLZ Spielort','wird für Navigation verwendet (KM Geld Schiedsrichter)'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['org_place']        = ['Ort Spielort','wird für Navigation verwendet (KM Geld Schiedsrichter)'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['org_lat']        = ['Breitengrad','Wird beim speichern automatisch berechnet'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['org_lng']        = ['Längengrad','Wird beim speichern automatisch berechnet'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['download']        = ['Aufgebot','Spesenabrechnung'];
$GLOBALS['TL_LANG']['tl_sf_rounds']['reg_closed']        = ['Schiedsrichterregistrierung Abgeschlossen?','Keine weiteren Schiedsrichter können sich anmelden. Anzeige im Spielplan grün.'];
