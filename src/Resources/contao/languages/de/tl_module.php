<?php

$GLOBALS['TL_LANG']['tl_module']['season'] 			= ["Anmeldung für Saison","Anmeldeformular für die Saison"];
$GLOBALS['TL_LANG']['tl_module']['bln_register']	    = ["Registrierung möglich?","Wenn aktiv, können Schiedsrichter sich für ein Spiel registrieren."];
$GLOBALS['TL_LANG']['tl_module']['bln_view_by_cat']	    = ["Anzeige Gruppiert nach Kateogrie?","Default: nach Datum und Kategorie"];
$GLOBALS['TL_LANG']['tl_module']['bln_hide_played_rounds']	    = ["Gespielte Runden in der Registrierung ausblenden?",""];
