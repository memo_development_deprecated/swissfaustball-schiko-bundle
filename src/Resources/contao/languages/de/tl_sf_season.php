<?php


$GLOBALS['TL_LANG']['tl_sf_season']['schiko_legend'] = 'SCHIKO - Datenbank';
$GLOBALS['TL_LANG']['tl_sf_season']['schiko_driving_expenses'] = ['Fahrspesen CHF/Km','Fahrspesen pro Kilometer in CHF als Dezimal Zahl z.Bsp: 0.5'];
$GLOBALS['TL_LANG']['tl_sf_season']['xlsTotals']        = ['Einsatzplan PDF',''];
$GLOBALS['TL_LANG']['tl_sf_season']['schiko_send_decline_mail']        = ['Ablehnen Mail senden?','Wenn eine Anfrage abgelehnt wird, wird der Schiedsrichter per E-Mail benachrichtigt.'];
$GLOBALS['TL_LANG']['tl_sf_season']['schiko_decline_notification']        = ['Benachrichtigung Ablehnung',''];
$GLOBALS['TL_LANG']['tl_sf_season']['xlsInsets']        = 'XLS Download Schiedsrichter Einsätze';
$GLOBALS['TL_LANG']['tl_sf_season']['xlsSeasonTotals']        = 'XLS Download – Saisonabrechnung';
$GLOBALS['TL_LANG']['tl_sf_season']['pdfInsetPlan']        = 'PDF Download – Einsatzplan';
