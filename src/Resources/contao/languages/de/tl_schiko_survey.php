<?php


$GLOBALS['TL_LANG']['tl_schiko_survey']['survey_legend']   = 'Umfrage';
$GLOBALS['TL_LANG']['tl_schiko_survey']['expert_legend'] 				= "Experteneinstellungen";
$GLOBALS['TL_LANG']['tl_schiko_survey']['season']   = ['Saison'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['category'] = ['Kategorie'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['round']    = ['Runde'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['referee']  = ['Schiedsrichter'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['status']   = ['Status'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['function'] = ['Funktion'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['fee']      = ['Taggeld'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['travel_cost']   = ['Reisespesen'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['porto']    = ['Porto'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['total']    = ['Total CHF','Total Entschädigung'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['bonus'] 				        = ["Bonus CHF","Wird beim Taggeld addiert."];
$GLOBALS['TL_LANG']['tl_schiko_survey']['catering'] = ['Verpflegung'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['cssID'] 				        = ["CSS-ID/Klasse","Hier können Sie eine ID und beliebig viele Klassen eingeben."];

$GLOBALS['TL_LANG']['tl_schiko_survey']['status_options'] = ['possible'=>'reserviert','booked'=>'gebucht','cancel'=>'abgelehnt'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['function_options'] = ['manager'=>'Spielleiter','referee'=>'Schiedsrichter','linesman'=>'Linienrichter'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['travel_distance'] = ['Reisedistanz in Km','Wird automatisch berechnet. Grundlage für Reisespesen. (Distanz A-B * 2)'];
$GLOBALS['TL_LANG']['tl_schiko_survey']['calcTravelDistance'] 		= ["Reisedistanz neu berechnen?", "Erzwingt die Berechnung der Reisedistanz + Reisespesen"];
