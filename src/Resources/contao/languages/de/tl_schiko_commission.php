<?php


$GLOBALS['TL_LANG']['tl_schiko_commission']['commission_legend'] 			= "Kommission";
$GLOBALS['TL_LANG']['tl_schiko_commission']['expert_legend'] 				= "Experteneinstellungen";
$GLOBALS['TL_LANG']['tl_schiko_commission']['invisible_legend'] 			= "Sichtbarkeit";
$GLOBALS['TL_LANG']['tl_schiko_commission']['commission_contact_legend'] 			= "Kommissionsverantwortlicher";

$GLOBALS['TL_LANG']['tl_schiko_commission']['firstname'] 				        = ["Vorname"];
$GLOBALS['TL_LANG']['tl_schiko_commission']['lastname'] 				        = ["Nachname"];
$GLOBALS['TL_LANG']['tl_schiko_commission']['email'] 				        = ["E-Mail"];
$GLOBALS['TL_LANG']['tl_schiko_commission']['name'] 				        = ["Name"];
$GLOBALS['TL_LANG']['tl_schiko_commission']['shortname'] 				        = ["Kürzel",'Bsp. F-KO'];
$GLOBALS['TL_LANG']['tl_schiko_commission']['description'] 				    = ["Beschreibung"];
$GLOBALS['TL_LANG']['tl_schiko_commission']['published'] 				    = ["Veröffentlichen","Das Element auf der Webseite  anzeigen."];
$GLOBALS['TL_LANG']['tl_schiko_commission']['cssID'] 				        = ["CSS-ID/Klasse","Hier können Sie eine ID und beliebig viele Klassen eingeben."];
$GLOBALS['TL_LANG']['tl_schiko_commission']['member'] 				        = ["Schiko Verantwortlicher","Ein, oder mehrere Personen, welche per E-Mail über Stati benachrichtigt werden."];
