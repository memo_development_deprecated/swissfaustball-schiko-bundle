<?php


$GLOBALS['TL_LANG']['tl_member']['referee_legend'] 			= "Schiedsrichter";
$GLOBALS['TL_LANG']['tl_member']['address_legend'] 				= "Adresse";
$GLOBALS['TL_LANG']['tl_member']['koordinates_legend'] 				= "Koordinaten Heimadresse";
$GLOBALS['TL_LANG']['tl_member']['stats_legend'] 				= "Statistik";

$GLOBALS['TL_LANG']['tl_member']['email'] 			= ["E-Mail"];
$GLOBALS['TL_LANG']['tl_member']['phone_business'] 	= ["Telefon Geschäft"];
$GLOBALS['TL_LANG']['tl_member']['mobile'] 	= ["Telefon Mobil"];
$GLOBALS['TL_LANG']['tl_member']['phone'] 	= ["Telefon Privat"];
$GLOBALS['TL_LANG']['tl_member']['street'] 			= ["Adresse"];
$GLOBALS['TL_LANG']['tl_member']['place'] 			= ["Ort"];
$GLOBALS['TL_LANG']['tl_member']['coordinates'] 			= ["Breitengrad/Längengrad","Für Berechnung des Standortes/Spesen (Wird automatisch gesetzt wenn leer)"];
$GLOBALS['TL_LANG']['tl_member']['rel_tl_mod_club'] = ["Club"];
$GLOBALS['TL_LANG']['tl_member']['club']            = ["Verein"];
$GLOBALS['TL_LANG']['tl_member']['count_for_club']  = ["Einsätze zählen für Verein"];
$GLOBALS['TL_LANG']['tl_member']['brevet_year']	    = ["Brevet Jahr"];
$GLOBALS['TL_LANG']['tl_member']['entry_field']	    = ["Einsätze im Feld"];
$GLOBALS['TL_LANG']['tl_member']['entry_hall'] 	    = ["Einsätze in der Halle"];
$GLOBALS['TL_LANG']['tl_member']['entry_total']	    = ["Total Einsätze"];
$GLOBALS['TL_LANG']['tl_member']['entry_last'] 	    = ["Letzter Einsatz"];
$GLOBALS['TL_LANG']['tl_member']['schiko_tags']	    = ["Schiedsrichter Kategorie"];
$GLOBALS['TL_LANG']['tl_member']['singleSRC']	    = ["Portrait für ID-Card"];
