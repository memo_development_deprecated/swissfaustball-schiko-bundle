<?php



$GLOBALS['TL_LANG']['tl_sf_categories']['schiko_legend'] = 'SCHIKO - Datenbank';
$GLOBALS['TL_LANG']['tl_sf_categories']['schiko_commission'] = ['Kommission'];
$GLOBALS['TL_LANG']['tl_sf_categories']['schiko_referee_count'] = ['Anzahl Schiedsrichter'];
$GLOBALS['TL_LANG']['tl_sf_categories']['schiko_rate'] = ['Tarife'];
$GLOBALS['TL_LANG']['tl_sf_categories']['sr_einsatz'] = ['Einsatz'];
$GLOBALS['TL_LANG']['tl_sf_categories']['sr_value'] = ['Taggeld in CHF'];
$GLOBALS['TL_LANG']['tl_sf_categories']['sr_food'] = ['Verpflegung'];
$GLOBALS['TL_LANG']['tl_sf_categories']['snack'] = ['Zwischenverpflegung'];
$GLOBALS['TL_LANG']['tl_sf_categories']['meal'] = ['Volle Verpflegung'];
$GLOBALS['TL_LANG']['tl_sf_categories']['half'] = ['1/2 Tag'];
$GLOBALS['TL_LANG']['tl_sf_categories']['full'] = ['1 Tag'];
