<?php

$GLOBALS['TL_LANG']['tl_settings']['schiko_legend'] 			= "SCHIKO Einstellungen";
$GLOBALS['TL_LANG']['tl_settings']['schiko_google_maps_api_key'] 		= ["Google Maps API Key", "Wird für die Geocoding funktion benötigt (Distance + Location)"];
$GLOBALS['TL_LANG']['tl_settings']['schiko_openroute_key'] 		= ["Openroute Service API Key", "Wird für die Geocoding funktion benötigt (Distance + Location)"];
