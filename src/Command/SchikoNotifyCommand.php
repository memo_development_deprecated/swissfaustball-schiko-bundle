<?php

namespace Memo\SchikoBundle\Command;

use Contao\CoreBundle\Framework\ContaoFramework;
use Doctrine\DBAL\Connection;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use Memo\SchikoBundle\Service\ExportService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use NotificationCenter\Model\Notification;
use Psr\Log\LogLevel;
use Contao\CoreBundle\Monolog\ContaoContext;
use Symfony\Component\HttpFoundation\RequestStack;

class SchikoNotifyCommand extends Command {

    //php contao-console app:get-webcamimages
    protected static $defaultName = 'schiko:sendnotifications';
    protected $framework;
    protected $output;
    protected $db;
    private $aCategoryRecipients;

    public function __construct(ContaoFramework $framework,Connection $db)
    {
        $this->framework = $framework;
        $this->db = $db;
        $this->framework->initialize();
        parent::__construct();
    }


    protected function configure()
    {
        $this
            ->setDescription('Send SCHIKO Notifications')
            ->setHelp('This Command send SCHIKO Notifications to members');
    }

    protected function execute(InputInterface $input,OutputInterface $output)
    {
        $this->output = $output;
        $this->output->writeln([
            '============',
            'Send Notification to SCHIKO',
            '============'
        ]);
        $this->_sendSchikoNotification();

        $this->output->writeln([
            '',
            '============',
            'Send Schiedsrichter Confirmation Notifications',
            '============',

        ]);
        $this->_sendRoundConfirmation();
        return 1;
    }

    /**
     * @param int $prmNotificationId
     * @return bool
     * @throws \Exception
     */
    private function _sendRoundConfirmation($prmNotificationId = 7) {
        $oRound  = SfRoundsModel::findOneBy(['sent=?'],[1]);

        if(null === $oRound)
        {
            $this->output->writeln(['- no round found.. | nothing to do']);
            return 0;
        }

        //Get Referees (only booked) + Organizer in Round
        $oSurvey    = SchikoSurveyModel::findBy(['round=?','status=?'],[$oRound->id,'booked']);
        $oOrganizer = $oRound->getRelated('schiko_organizer');

        //Create Entry PDF

        $exService  = new ExportService($this->framework,new RequestStack(),$this->db);
        $tmpDir     = \System::getContainer()->getParameter('kernel.project_dir')."/files/einsatzplan";

        if(!is_dir($tmpDir)) {
            mkdir($tmpDir,0755);
            if(!is_writeable($tmpDir.'/dummy.txt')){chmod($tmpDir,0755);}
            if(is_file($tmpDir.'/dummy.txt')){
                unlink($tmpDir.'/dummy.txt');
            }
        }

        $tmpFileName = $tmpDir."/".\StringUtil::sanitizeFileName(sprintf("%sswissfaustball_einsatzplan-%s.pdf",date('ymd_',$oRound->date),$oRound->location));

        $aToken     = [];
        $tmpFile    = $tmpFileName;
        $exService->exportOfferPDF($oRound->id,$tmpFile);
        if(!is_file($tmpFile)) {
            $this->output->writeln(['- cant\' create PDF 4 round: '.$oRound->id]);
            $oRound->sent = 9; //Fehler beim Versand
            $oRound->save();
            return 0;
        }
//        $arrAttachements['form_attachement'] = $tmpFile;
        $aToken['form_attachement'] = $tmpFile;

        //Prepare Notification
        $objNotification    = Notification::findByPk($prmNotificationId);
        if (null === $objNotification) {
            $this->output->writeln(["- can\'t send Confirm Mails to referees | notification not found"]);
            return 0;
        }

        $oCategory  = $oRound->getRelated('f_category');
        $aToken['form_round_name'] = date('d. m. Y',$oRound->date)." / ".$oRound->location;
        $aToken['form_round_date'] = date('d. m. Y',$oRound->date);
        $aToken['form_category']  =  $oCategory->category;


        //Send Mail to Organizer
        $oRound->sent = 5; //send in progress
        $oRound->save();
        if(null !== $oOrganizer) {
            $aToken['form_recipient']        = empty($oOrganizer->contactEmail)? '' : $oOrganizer->contactEmail;
            $aToken['form_recipient_firstname'] = $oOrganizer->contactFirstname;
            $aToken['form_recipient_lastname'] = $oOrganizer->contactName;
            $this->output->writeln(['- send Mail to organizer ' . $oOrganizer->contactFirstname . " " . $oOrganizer->contactName]);
            if(!empty($aToken['form_recipient'])) {
                $objNotification->send($aToken, 'de');
            }
        }

        //Mail to Referees
        foreach($oSurvey as $key => $item) {
            $referee = $item->getRelated('referee');
            $aToken['form_recipient']        = empty($referee->email)? '' : $referee->email;
            $aToken['form_recipient_firstname'] = $referee->firstname;
            $aToken['form_recipient_lastname'] = $referee->lastname;
            $this->output->writeln(['- send Mail to referee ' . $referee->firstname . " " . $referee->lastname]);
            if(!empty($aToken['form_recipient'])) {
                $objNotification->send($aToken, 'de');
            }
        }

        $oRound->sent = 2; //Versand erfolgreich
        $oRound->save();
        return 1;
    }


    /**
     * @param $prmNotificationId
     * @return void
     * @throws \Exception
     */
    private function _sendSchikoNotification($prmNotificationId = 6) {
        $aData              = $this->_getSurveyRecipientsByState();
        $objNotification    = Notification::findByPk($prmNotificationId);
        $arrTokens          = [];

        if(null !== $aData) {
            foreach($aData as $category_id => $aCategories) {
                //Generate HTML Table with all Members
                $template = new \FrontendTemplate('schiko_notify_commission');
                $template->aSurvey = $aCategories['survey'];

                /* Anmeldungen von Schiedsrichtern */
                $sTable = $template->parse();

                //Mailto an ale Schiko verantwortliche
                $iCount = 0;
                foreach ($aCategories['aCategoryRecipients'] as $key => $recipient) {
                    if(empty($recipient)) {
                        $this->output->writeln(['- no recipient found..']);
                        continue;
                    }
                    $arrTokens['form_commission']           = $aCategories['commission'];
                    $arrTokens['form_recipient_email']      = $recipient->email;
                    $arrTokens['form_recipient_firstname']  = $recipient->firstname;
                    $arrTokens['form_recipient_lastname']   = $recipient->lastname;
                    $arrTokens['form_member_table']         = $sTable;

                    if (null !== $objNotification) {
                        $this->output->writeln(['- parse notification 4 ' . $recipient->firstname . " " . $recipient->lastname]);
                        $objNotification->send($arrTokens);
                        $iCount++;
                    }else{
                        $this->output->writeln(['- can\'t send mail to: '. $recipient->firstname . " " . $recipient->lastname." | notification not found"]);
                    }
                }

                $this->output->writeln(['-- send: '. $iCount . ' E-Mails in Category:'.$category_id]);
            }
        }
    }


    /**
     * sucht alle Schiedsrichter um eine E-Mail mit den bestätigten Terminen zu senden.
     * @param string $prmState
     * @param int $intNotifyState
     * @return array|void
     * @throws \Exception
     */
    private function _getSurveyRecipientsByState(string $prmState = 'possible', int $intNotifyMemberState = 0) {
        $aReturn = [];
        $oSurvey = SchikoSurveyModel::findBy(['status=?','notify_member=?'],[strtolower($prmState),$intNotifyMemberState]);

        if(null !== $oSurvey) {
            foreach($oSurvey as $key => $val) {
                $data = $val;
                //Send Set Status
                $val->notify_member = 1;
                $val->save();

                //Get Round & Category
                $data->round            = SfRoundsModel::findByPk($val->round);
                $data->category_name    = $data->round->getRelated('f_category')->category;
                $data->commission       = $data->round->getRelated('f_category')->getRelated('schiko_commission');
                $aReturn[$val->category]['survey'][] = $data;

                //add Recipients per Category
                if(null !== $data->commission) {
                    $aReturn[$val->category]['aCategoryRecipients'] = $data->commission->getRelated('member');
                    $aReturn[$val->category]['commission'] = $data->commission->name;
                }else {
                    $aReturn[$val->category]['aCategoryRecipients'] = [];
                    $aReturn[$val->category]['commission'] = '';
                }

            }
            return $aReturn;
        }
    }

}
