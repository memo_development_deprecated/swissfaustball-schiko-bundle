<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */


namespace Memo\SchikoBundle\Controller;

use Contao\CoreBundle\Controller\AbstractController;
use Contao\CoreBundle\Framework\ContaoFramework;
use Doctrine\DBAL\Connection;
use Memo\ModSwissfaustballBundle\Model\SfRoundsModel;
use Memo\SchikoBundle\Service\ExportService;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Contao\BackendUser;

class BackendController extends AbstractController
{

    private $framework;
    private $security;
    private $db;

    public function __construct(ContaoFramework $framework,Security $security, Connection $db)
    {
        $this->framework = $framework;
        $this->framework->initialize();
        $this->security = $security;
        $this->db = $db;
    }

    /**
     * @param int $round_id
     * @return false|void
     */
    public function activateRoundMailing(int $round_id) {
        if(null === $round_id) {
            return false;
        }
        $objUser = BackendUser::getInstance();
        if(null !== $objUser) {
            $oRound = SfRoundsModel::findByPk($round_id);
            $oRound->sent = 1;
            $oRound->reg_closed = 1;
            $oRound->save();
        }
        \Controller::redirect('contao?do=tl_sf_rounds');
    }


    /**
     * @param int $round_id
     * @return false|void
     */
    public function deactivateRoundMailing(int $round_id) {
        if(null === $round_id) {
            return false;
        }
        $objUser = BackendUser::getInstance();
        if(null !== $objUser) {
            $oRound = SfRoundsModel::findByPk($round_id);
            $oRound->sent = 0;
            $oRound->reg_closed = 0;
            $oRound->save();
        }
        \Controller::redirect('contao?do=tl_sf_rounds');
    }

    /**
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function exportRefereeXLS()
    {
        $exService  = new ExportService($this->framework,new RequestStack(),$this->db);
        $exService->exportDirectoryXLS();
    }

    /**
     * @return void
     */
    public function exportRefereeIdCard(RequestStack $request)
    {
        $user = $request->getCurrentRequest()->query->get('user');
        if(empty($user))
        {
            return \Controller::redirect('contao?do=member');
        }
        $exService  = new ExportService($this->framework,$request,$this->db);
        return $exService->exportRefereeIdCard($user);
    }

}
