<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\SchikoBundle\Controller;

use Contao\CoreBundle\Framework\ContaoFramework;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\ModSwissfaustballBundle\Model\SfSeasonModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use Memo\SchikoBundle\Service\ExportService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Contao\FrontendUser;

class SchikoExportController extends AbstractController
{
    private $framework;
    private $exportService;

    public function __construct(ContaoFramework $framework,ExportService $exportService)
    {
        $this->framework = $framework;
        $this->framework->initialize();
        $this->exportService = $exportService;
    }

    /**
     * @param $season_id
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function exportEntryPDF($season_id = 'current') {

        //Get newest active season
        if($season_id === 'current') {
            $oSeason = SfSeasonModel::findOneBy(['active = ?'],[1],['order'=>'tstamp desc']);
            $season_id = $oSeason->id;
        }

        if(null !== $season_id) {
            return $this->exportService->exportEntryPDF($season_id);
        }

        \Controller::redirect(\Controller::getReferer());
    }


    /**
     * @return bool
     */
    public function exportDirectoryPDF() {
        return $this->exportService->exportDirectoryPDF();
        \Controller::redirect(\Controller::getReferer());
    }

    /**
     * @return bool
     */
    public function exportDirectoryXLS() {
        return $this->exportService->exportDirectoryXLS();
        \Controller::redirect(\Controller::getReferer());
    }

    /**
     * @return bool
     */
    public function exportXlsInsets(int $season_id) {
        return $this->exportService->exportXlsInsets($season_id);
        \Controller::redirect(\Controller::getReferer());
    }

    /**
     * @return bool
     */
    public function exportOfferPDF(int $round) {
        return $this->exportService->exportOfferPDF($round);
        \Controller::redirect(\Controller::getReferer());
    }


    /**
     * @param int $season_id
     * @return void
     */
    public function exportSeasonBillingXLS(int $season_id)
    {
        if(empty($season_id))
        {
            \Controller::redirect(\Controller::getReferer());
        }
        return $this->exportService->exportSeasonBillingXLS($season_id);
        \Controller::redirect(\Controller::getReferer());
    }


}
