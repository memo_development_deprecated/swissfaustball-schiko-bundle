<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2015 Media Motion AG
 *
 * @package   SCHIKO Bundle
 * @author    Christian Nussbaumer, Media Motion AG
 * @license   MEMO
 * @copyright Media Motion AG
 */

namespace Memo\SchikoBundle\Controller;

use Contao\CoreBundle\Framework\ContaoFramework;
use Memo\ModSwissfaustballBundle\Model\SfCategoriesModel;
use Memo\SchikoBundle\Model\SchikoSurveyModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Contao\FrontendUser;

class SchikoAjaxController extends AbstractController
{
    private $framework;

    public function __construct(ContaoFramework $framework)
    {
        $this->framework = $framework;
        $this->framework->initialize();
    }


    /**
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse {
        $aReturn    = ['success'=>false];
        $aRecord    = new SchikoSurveyModel();
        $user       = FrontendUser::getInstance();

        if(!empty($user) && !empty($request->request->get('categorie')) && !empty($request->request->get('season')) && $request->request->get('round')) {
            $oCategory = SfCategoriesModel::findByPk($request->request->get('categorie'));
//            $rates     = \StringUtil::deserialize($oCategory->schiko_rate);

            $aRecord->tstamp    = time();
            $aRecord->season    = $request->request->get('season');
            $aRecord->category  = $request->request->get('categorie');
            $aRecord->round     = $request->request->get('round');
            $aRecord->referee   = $user->id;
            $aRecord->status    = 'possible';

            if ($aRecord->save()) {
                $aReturn = ['success' => true,'surveyId'=>$aRecord->id];
            }
        }

        return new JsonResponse($aReturn);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function unregister(Request $request): JsonResponse {
        $user       = FrontendUser::getInstance();
        $aReturn = ['success' => false];

        //Validate Post
        if(!empty($user) && !empty($request->request->get('surveyId'))) {
            $aRecord = SchikoSurveyModel::findByPk($request->request->get('surveyId'));

            //Validate and check access
            if (!empty($aRecord) && $aRecord->referee == $user->id && $aRecord->delete()) {
                $aReturn = ['success' => true];
            }
        }

        return new JsonResponse($aReturn);
    }

}
