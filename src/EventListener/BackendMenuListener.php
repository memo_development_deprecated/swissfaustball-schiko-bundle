<?php


// src/EventListener/BackendMenuListener.php
namespace Memo\SchikoBundle\EventListener;

use App\Controller\BackendController;
use Contao\CoreBundle\Event\MenuEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Terminal42\ServiceAnnotationBundle\Annotation\ServiceTag;

/**
 * @ServiceTag("kernel.event_listener", event="contao.backend_menu_build", priority=-255)
 */
class BackendMenuListener extends ServiceTag
{
    protected $router;
    protected $requestStack;

    public function __construct(RouterInterface $router, RequestStack $requestStack)
    {
        $this->router = $router;
        $this->requestStack = $requestStack;
    }

    public function __invoke(MenuEvent $event): void
    {
        $factory = $event->getFactory();
        $tree    = $event->getTree();

        if ('mainMenu' !== $tree->getName()) {
            return;
        }

        $contentNode = $tree->getChild('memo_schiko');
        
        $node = $factory
            ->createItem('export_referee')
            ->setUri('/contao/schiko/referee/export')
            ->setLabel($GLOBALS['TL_LANG']['MOD']['export_referee'][0])
            ->setLinkAttribute('title', $GLOBALS['TL_LANG']['MOD']['export_referee'][0])
            ->setLinkAttribute('class', 'navigation export_referee')
            ->setCurrent($this->requestStack->getCurrentRequest()->get('_controller') === BackendController::class)
        ;

        if(!is_null($contentNode) && !is_null($node)){
            $contentNode->addChild($node);
        }
        
    }
}